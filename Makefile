
out: out.o lib.o
	gcc -o out out.o lib.o

lib.o: lib.c
	gcc -o lib.o lib.c -c

out.o: out.S
	as -o out.o out.S -c

out.S: test.txt test
	./test test.txt out.S

test: main.o lex.o yacc.o ast.o map.o util.o codestream.o codegen.o assemblygen.o addrdesc.o registry.o types.o automaton.o smartarray.o nfabuilder.o set.o autoconvert.o
	gcc -o test main.o lex.o yacc.o ast.o map.o util.o codestream.o codegen.o assemblygen.o addrdesc.o registry.o types.o automaton.o smartarray.o nfabuilder.o set.o autoconvert.o -lm

main.o: main.c util.h prettyprint.h types.h y.tab.h
	gcc -o main.o main.c -c

set.o: set.h set.c
	gcc -o set.o set.c -c

autoconvert.o: autoconvert.c
	gcc -o autoconvert.o autoconvert.c -c

types.o: types.c types.h
	gcc -o types.o types.c -c

registry.o: registry.c registry.h types.h
	gcc -o registry.o registry.c -c

automaton.o: automaton.c automaton.h
	gcc -o automaton.o automaton.c -c

nfabuilder.o: nfabuilder.c automaton.h
	gcc -o nfabuilder.o nfabuilder.c -c

smartarray.o: smartarray.c smartarray.h
	gcc -o smartarray.o smartarray.c -c

codegen.o: codegen.c codestream.h types.h
	gcc -o codegen.o codegen.c -c

addrdesc.o: addrdesc.c addrdesc.h types.h
	gcc -o addrdesc.o addrdesc.c -c

assemblygen.o: assemblygen.c codestream.h types.h
	gcc -o assemblygen.o assemblygen.c -c

codestream.o: codestream.c codestream.h types.h
	gcc -o codestream.o codestream.c -c

util.o: util.c util.h types.h
	gcc -o util.o util.c -c

map.o: hashmap.c hashmap.h prettyprint.h types.h
	gcc -o map.o hashmap.c -c

ast.o: ast.c ast.h types.h
	gcc -o ast.o ast.c -c

yacc.o: y.tab.c ast.h types.h
	gcc -o yacc.o y.tab.c -c -g

y.tab.h y.tab.c: language.y
	yacc language.y -d -g

lex.o: lex.yy.c y.tab.h
	gcc -o lex.o lex.yy.c -c

lex.yy.c: language.l
	lex language.l
