# Small project experimenting with compiler-design

## Automaten:
Um die Automaten zu benutzen einfach einmal mit

    make test
compilieren. Dann

    ./test "regex" "String zu testen"
ausführen.

Die verschiedenen Stufen des Graphen landen in den Dateien *nfa.dot* für den nicht-deterministischen und nicht vereinfachten Automaten, *new.dot* für den nicht-deterministiechen vereinfachten (nicht minimalen) automaten und *dfa.dot* für den deterministischen Automaten (Fehlerzustände werden nicht mitangezeigt und auch nicht gespeichert).
 
Der Quellcode für die Automaten liegt in den Dateien: *automaton.c*, *automaton.h*, *autoconvert.c*, *nfbuilder.c*.

### Regex Syntax

Die Syntax der Regex ist sehr stark an die Mathematische angelegt. Ein paar kleine Erweiterung sind aber trotzdem dabei:

- $$[AZ] = (A|B|...|Z)$$
- $$\Sigma = \left\{x \in \mathbb{N} : x < 256\right\} =  \text{ASCII-Charaktere}$$

## Automata:
If you are just interested in the implementation I used for Non-deterministic and deterministic final automata, all code you want to see is int automaton.c, automaton.h and nfabuilder.c.

Running the compiler will save the automata for the regex in both its forms. In nfa.dot it is saved unsiplified and in new.dot it is simplified (though not minimal).
I am still currently working on the conversion from non-deterministic to deterministic automata so all automata saved are non-deterministic.

## Help:

### compiling the compiler:
To build the compiler and compile a test program just run
     
    make

### compiling code:
Once the compiler is compiled to use it use

    ./test inputFile outputFile
Then use an assembler to assemble and a linker to link against lib.o . 

## Calling external code:
As the syntax used by the compiler is very basic and nearly no library functions are implemented, you may want to call external code. To do so you can just use standard C practice and link against
the compiled objects (As for C++ please declare your functions as extern "C"). Please note you may need to use gcc as a linker if you need C-library functions.
