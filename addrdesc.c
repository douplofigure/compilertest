#include "addrdesc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "registry.h"
#include "prettyprint.h"

extern unsigned int getHash(char * str);
extern int getNextPrime(int n);
extern int isPrime(int n);

typedef struct location_stack_t {

    LOCATION loc;
    struct location_stack_t * prev;

} LOCATION_STACK;

typedef struct reg_entry_t {

    long regNum;
    struct reg_entry_t * next;
    LOCATION_STACK * locations;

} REG_ENTRY;

typedef struct symbol_entry_t {

    char * symbol;
    LOCATION_STACK * locations;
    struct symbol_entry_t * next;


} SYMBOL_ENTRY;

struct addr_desc_t {

    int mapSize;
    int entryCount;
    REG_ENTRY * regEntries;
    SYMBOL_ENTRY ** symEntries;

};

ADDR_DESC * createAddressDescriptor(int symCount) {

    ADDR_DESC * desc = malloc(sizeof(ADDR_DESC));
    desc->mapSize = getNextPrime(symCount);
    desc->entryCount = 0;
    desc->regEntries = NULL;
    desc->symEntries = malloc(sizeof(SYMBOL_ENTRY*) * desc->mapSize);

    for (int i = 0; i < desc->mapSize; ++i) {
        desc->symEntries[i] = NULL;
    }

    return desc;

}

LOCATION_STACK * locationStackPush(LOCATION_STACK * locStack, LOCATION loc) {

    LOCATION_STACK * newFrame = malloc(sizeof(LOCATION_STACK));
    newFrame->loc = loc;
    newFrame->prev = locStack;

    return newFrame;

}

void addressDescriptorSymbolPush(ADDR_DESC * desc, char * symbol, LOCATION location) {

    unsigned int hash = getHash(symbol);
    unsigned int pos = hash % desc->mapSize;

    if (desc->symEntries[pos]) {

        SYMBOL_ENTRY * entry = desc->symEntries[pos];
        do {
            if (!strcmp(entry->symbol, symbol)) {

                entry->locations = locationStackPush(entry->locations, location);
                return;

            }

        } while(entry = entry->next);

    }

    SYMBOL_ENTRY * entry = malloc(sizeof(SYMBOL_ENTRY));
    entry->locations = locationStackPush(NULL, location);
    entry->next = desc->symEntries[pos];
    entry->symbol = symbol;

    desc->symEntries[pos] = entry;
    desc->entryCount++;

}

void addressDescriptorTemporaryPush(ADDR_DESC * desc, long regNum, LOCATION location) {

    REG_ENTRY * entry = desc->regEntries;
    if (entry) {
        do {

            if (entry->regNum == regNum) {

                entry->locations = locationStackPush(entry->locations, location);
                return;

            }

        } while (entry = entry->next);
    }
    entry = malloc(sizeof(REG_ENTRY));
    entry->locations = locationStackPush(NULL, location);
    entry->next = desc->regEntries;
    entry->regNum = regNum;

    desc->regEntries = entry;

}

void addressDescriptorPush(ADDR_DESC * desc, CODE_ARG arg, LOCATION loc) {

    switch (arg.type) {

        case ARG_SYMBOL:
            addressDescriptorSymbolPush(desc, arg.value.name, loc);
            break;

        case ARG_REF:
        case ARG_REG:
            addressDescriptorTemporaryPush(desc, arg.value.num, loc);
            break;

        case ARG_CONSTANT:
            break;

        default:
            break;


    }

}

LOCATION addressDescriptorSymbolGet(ADDR_DESC * desc, char * symbol) {

    unsigned int hash = getHash(symbol);
    unsigned int pos = hash % desc->mapSize;

    if (desc->symEntries[pos]) {
        SYMBOL_ENTRY * entry = desc->symEntries[pos];
        do {
            if (entry->symbol && !strcmp(entry->symbol, symbol)) {
                return entry->locations->loc;
            }
        } while (entry = entry->next);

    }
    return (LOCATION) {0};

}

LOCATION addressDescriptorTemporaryGet(ADDR_DESC * desc, long regNum) {

    REG_ENTRY * entry = desc->regEntries;
    if (!entry) return (LOCATION) {0};
    do {

        if (entry->regNum == regNum) {

            return entry->locations->loc;

        }

    } while (entry = entry->next);

    return (LOCATION) {0};

}

LOCATION addressDescriptorGet(ADDR_DESC * desc, CODE_ARG arg) {

    switch (arg.type) {

        case ARG_SYMBOL:
            return addressDescriptorSymbolGet(desc, arg.value.name);

        case ARG_REF:
        case ARG_REG:
            return addressDescriptorTemporaryGet(desc, arg.value.num);

        case ARG_CONSTANT:
            break;

        default:
            break;


    }

    return (LOCATION) {0};

}

LOCATION addressDescriptorSymbolPop(ADDR_DESC * desc, char * symbol) {

    unsigned int hash = getHash(symbol);
    unsigned int pos = hash % desc->mapSize;

    if (desc->symEntries[pos]) {
        SYMBOL_ENTRY * entry = desc->symEntries[pos];
        do {
            if (!strcmp(entry->symbol, symbol)) {
                if (!entry->locations->prev) return entry->locations->loc;
                LOCATION loc = entry->locations->loc;
                LOCATION_STACK * frame = entry->locations;

                entry->locations = frame->prev;
                free(frame);

                return loc;
            }
        } while (entry = entry->next);

    }
    return (LOCATION) {0};

}

LOCATION addressDescriptorTemporaryPop(ADDR_DESC * desc, int regNum) {

    REG_ENTRY * entry = desc->regEntries;
    if (!entry) return (LOCATION) {0};
    do {

        if (entry->regNum == regNum) {
            if (!entry->locations) return (LOCATION) {0};
            if (!entry->locations->prev) return entry->locations->loc;
            LOCATION loc = entry->locations->loc;
            LOCATION_STACK * frame = entry->locations;

            entry->locations = frame->prev;

            return loc;

        }

    } while (entry = entry->next);

    return (LOCATION) {0};

}

LOCATION addressDescriptorPop(ADDR_DESC * desc, CODE_ARG arg) {

    switch (arg.type) {

        case ARG_SYMBOL:
            return addressDescriptorSymbolPop(desc, arg.value.name);

        case ARG_REG:
            return addressDescriptorTemporaryPop(desc, arg.value.num);

        case ARG_CONSTANT:
            break;

        default:
            break;


    }

}

char * locationGetName(LOCATION loc) {

    if (loc.isRef) {

        if (!loc.regNum)
            make_error(fprintf(stderr, "Can not create reference from memory\n"));

        //printf("(%%%s)\n", getRegistryName(loc.regNum-1));
        char * str = malloc(sizeof(char) * 6);
        for (int i = 0; i < 6; ++i) {
            str[i] = 0;
        }
        sprintf(str, "(%s)", getRegistryName(loc.regNum-1));
        return str;

    }

    if (loc.regNum) {

        return getRegistryName(loc.regNum-1);

    } else if (loc.varOffset) {

        char * str = malloc(sizeof(char) * 17);
        sprintf(str, "-%d(%%rbp)", loc.varOffset);
        return str;

    }
    if (!loc.label) {
        return "%rax";
        //make_error(fprintf(stderr, "Could not get name for location\n"));
    }

    char * res = malloc(sizeof(char) * (strlen(loc.label) + 7));

    sprintf(res, "%s(%%rip)", loc.label);

    return res;
}

void addressDescriptorClear(ADDR_DESC * desc) {

    while (desc->regEntries) {

        REG_ENTRY * entry = desc->regEntries->next;
        free(desc->regEntries);
        desc->regEntries = entry;

    }

    for (int i = 0; i < desc->mapSize; ++i) {

        while (desc->symEntries[i]) {

            SYMBOL_ENTRY * entry = desc->symEntries[i]->next;
            free(desc->symEntries[i]);
            desc->symEntries[i] = entry;

        }

    }

    desc->entryCount = 0;

}
