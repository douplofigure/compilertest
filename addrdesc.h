#ifndef ADDRDESC_H_INCLUDED
#define ADDRDESC_H_INCLUDED

#include "codestream.h"

typedef struct addr_desc_t ADDR_DESC;

typedef struct location_t {

    int regNum; /// reg + 1; 0 => not in registry
    int varOffset;
    char * label;
    int isRef;

} LOCATION;

ADDR_DESC * createAddressDescriptor(int expectedSymbolCount);
void addressDescriptorPush(ADDR_DESC * desc, CODE_ARG arg, LOCATION loc);
LOCATION addressDescriptorGet(ADDR_DESC * desc, CODE_ARG arg);
LOCATION addressDescriptorPop(ADDR_DESC * desc, CODE_ARG arg);

void addressDescriptorClear(ADDR_DESC * desc);

char * locationGetName(LOCATION loc);

#endif // ADDRDESC_H_INCLUDED
