#include "codegen.h"

#include <stdio.h>

#include "codestream.h"
#include "types.h"
#include "hashmap.h"
#include "prettyprint.h"

#include "addrdesc.h"

#include "registry.h"

typedef struct const_list_t {

    struct const_list_t * prev;
    CODE_ARG value;
    LOCATION loc;

} CONST_LIST;

static SYMBOL_TABLE * symbolTable;
static ADDR_DESC * addressDescriptor;

static CONST_LIST * constList;

void addConstToList(CODE_ARG arg, LOCATION origin) {

    CONST_LIST * elem = malloc(sizeof(CONST_LIST));
    elem->prev = constList;
    elem->value = arg;
    elem->loc = origin;

    constList = elem;

}

void writeConstList(FILE * file) {

    while (constList) {

        PRINT_TYPE pt;
        PRINT_VALUE pv = getPrintValue(constList->value.valType, &pt, (VALUE) constList->value.value.name);

        if (pt >= PT_STRING) {

        } else {

            int * tmp = (int*) &pv;
            fprintf(file, "\t.align %d\n%s:\n", 4 * pt, constList->loc.label);
            for (int i = 0; i < pt; ++i) {

                //printf("int=%d\n", *tmp);
                fprintf(file, "\t.long %d\n", *tmp);

                tmp++;

            }

        }

        CONST_LIST * tmpList = constList;

        constList = constList->prev;

        free(tmpList);

    }

}

char getTypeLengthSuffix(int length) {

    switch (length) {

        case 1:
            return 'b';

        case 2:
            return 'w';

        case 4:
            return 'l';

        case 8:
            return 'q';

    }

    make_error(fprintf(stderr, "Unknown type size %d\n", length))

}

char * getSignMoveSuffix(VALUE_TYPE orgType, VALUE_TYPE destType) {

    static char suffix[4];

    if (getTypeInfo(orgType.baseId).attributes&TA_FP_TYPE || getTypeInfo(destType.baseId).attributes&TA_FP_TYPE)
        return "sd";

    if (getTypeSize(destType) < getTypeSize(orgType)) {
        //make_error(fprintf(stderr, "Can not convert %d to %d\n", getTypeSize(orgType), getTypeSize(destType)));
        return "";
    }

    //if (getTypeSize(orgType) == getTypeSize(destType)) return "";

    suffix[0] = 's';
    suffix[1] = getTypeLengthSuffix(getTypeSize(orgType));
    suffix[2] = getTypeLengthSuffix(getTypeSize(destType));

    if (suffix[1] == suffix[2]){

        suffix[0] = suffix[1];
        suffix[1] = 0;

    }

    suffix[3] = 0;
    return suffix;

}

void moveConstantToLocation(FILE * file, CODE_ARG arg, LOCATION loc, VALUE_TYPE destType) {

    static int constCount;

    int valSize = getTypeSize(arg.valType);
    int isFp = isFloatingPoint(arg.valType);

    LOCATION origin;
    origin.regNum = 0;
    origin.varOffset = 0;
    origin.isRef = 0;

    if (isFp) {

        origin.label = malloc(sizeof(char) * 13);

        sprintf(origin.label, ".cf%d", constCount++);

        if (!loc.regNum || loc.isRef) {
            LOCATION tmp;
            tmp.regNum = getReg(arg.valType, arg)+1;
            tmp.varOffset = 0;
            tmp.label = NULL;
            tmp.isRef = 0;

            fprintf(file, "\tmovs%c %s, %s\n", valSize > 4 ? 'd' : 's', locationGetName(origin), locationGetName(tmp));
            fprintf(file, "\tmovs%c %s, %s\n", valSize > 4 ? 'd' : 's', locationGetName(tmp), locationGetName(loc));

        } else {
            fprintf(file, "\tmovs%c %s, %s\n", valSize > 4 ? 'd' : 's', locationGetName(origin), locationGetName(loc));
        }
        addConstToList(arg, origin);
        return;

    }

    /*if (valSize <= 4)  {

        //fprintf(file, "# loading value instantly\n");
        fprintf(file, "\tmov%s $%d, %s\n", getSignMoveSuffix(arg.valType, destType), arg.value.num, locationGetName(loc));
        return;

    }*/
    origin.label = malloc(sizeof(char) * 13);
    sprintf(origin.label, ".cl%d", constCount++);
    if (!loc.regNum || loc.isRef) {
        LOCATION tmp;
        tmp.regNum = getReg(arg.valType, arg)+1;
        tmp.varOffset = 0;
        tmp.label = NULL;
        tmp.isRef = 0;

        fprintf(file, "\tmov %s, %s\n", locationGetName(origin), locationGetName(tmp));
        fprintf(file, "\tmov %s, %s\n", locationGetName(tmp), locationGetName(loc));

        addConstToList(arg, origin);


    } else {
        //fprintf(file, "#in else condition\n");
        fprintf(file, "\tmov%s %s, %s\n", getSignMoveSuffix(arg.valType, destType), locationGetName(origin), locationGetName(loc));
        addConstToList(arg, origin);
    }



}

LOCATION moveValueToRegistry(FILE * file, CODE_ARG val, VALUE_TYPE destType) {

    LOCATION origin = addressDescriptorGet(addressDescriptor, val);

    if (origin.regNum && registryContains(origin.regNum-1, val)) {
        return origin;
    } else if (origin.regNum) {
        while (origin.regNum && val.type != ARG_REG) {

            addressDescriptorPop(addressDescriptor, val);
            origin = addressDescriptorGet(addressDescriptor, val);

        }
    }

    int regNum = getReg(destType, val);

    LOCATION dest;
    dest.regNum = regNum+1;
    dest.label = NULL;
    dest.varOffset = 0;
    dest.isRef = 0;
    switch (val.type) {

        case ARG_SYMBOL:
            fprintf(file, "\tmov%s %s, %s\n", getSignMoveSuffix(val.valType, destType), locationGetName(origin), locationGetName(dest));
            addressDescriptorPush(addressDescriptor, val, dest);
            registryUpdate(regNum, val);
            break;

        case ARG_CONSTANT:
            moveConstantToLocation(file, val, dest, destType);
            //addressDescriptorPush(addressDescriptor, val, dest);
            registryUpdate(regNum, val);
            break;


    }

    return dest;

}

int removeValueFromRegistry(FILE * file, int regNum) {


}

void forceValueRegistry(FILE * file, CODE_ARG arg, int regNum, VALUE_TYPE destType) {

    if (arg.type != ARG_CONSTANT) {
        LOCATION origin = addressDescriptorGet(addressDescriptor, arg);
        LOCATION dest;
        dest.regNum = regNum+1;
        dest.isRef = 0;
        fprintf(file, "\tmov%s %s, %s\n", getSignMoveSuffix(arg.valType, destType), locationGetName(origin), locationGetName(dest));
        registryUpdate(regNum, arg);
        //addressDescriptorPush(addressDescriptor, arg, dest);
    } else {

        LOCATION dest;
        dest.regNum = regNum+1;
        dest.isRef = 0;
        moveConstantToLocation(file, arg, dest, arg.valType);
        //fprintf(file, "\tmov $%d, %s\n", arg.value.num, locationGetName(dest));
        registryUpdate(regNum, arg);

    }

}

static unsigned int funcNum;
static char * theFuncName;

void writeFuncHeader(FILE * file, CODE_TUPLE tuple) {

    fprintf(file, "%s:\n", tuple.arg2.value.name);
    fprintf(file, ".LFS%d:\n", funcNum);
    fprintf(file, "\tpushq %%rbp\n\tmovq %%rsp, %%rbp\n");

    theFuncName = tuple.arg2.value.name;

    int symbolCount;
    SYMBOL_INFO * infoArray = symbolTableToArray(tuple.arg1.value.ptr, &symbolCount);
    char ** nameArray = symbolTableGetNameArray(tuple.arg1.value.ptr);

    wipeRegistries();
    symbolTable = tuple.arg1.value.ptr;
    addressDescriptorClear(addressDescriptor);

    int stackSize = 0;
    for (int i = 0; i < symbolCount; ++i) {

        LOCATION loc;
        loc.regNum = 0;

        loc.isRef = 0;
        while (stackSize % getTypeSize(infoArray[i].vType)) stackSize++;
        stackSize += getTypeSize(infoArray[i].vType);
        symbolTableUpdateOffset(tuple.arg1.value.ptr, nameArray[i], stackSize);

        loc.varOffset = stackSize;

        CODE_ARG arg;
        arg.type = ARG_SYMBOL;
        arg.valType = infoArray[i].vType;
        arg.value.name = nameArray[i];

        addressDescriptorPush(addressDescriptor, arg, loc);

    }

    while (stackSize % 16) stackSize++;

    fprintf(file, "\tsubq $%d, %%rsp\n", stackSize);
    stackSize = 0;

    AST * argDecl = tuple.res.value.ptr;
    int argc;
    AST ** argv = astGetChildren(argDecl, &argc);

    for (int i = 0; i < argc; ++i) {

        int tmp;
        char * name = astGetAttributeValue(astGetChildren(argv[i], &tmp)[0], A_VALUE).str;
        VALUE_TYPE valType = astGetAttributeValue(argv[i], A_VALUE_TYPE).vt;

        LOCATION loc;
        loc.regNum = 0;
        loc.isRef = 0;
        loc.varOffset = symbolTableLookup(tuple.arg1.value.ptr, name).memOffset;

        fprintf(file, "\tmov%s %s, %s\n", getSignMoveSuffix(valType, valType), getRegistryName(registryGetArgument(valType, i)), locationGetName(loc));
        CODE_ARG arg;
        arg.type = ARG_SYMBOL;
        arg.value.name = name;
        arg.valType = symbolTableLookup(tuple.arg1.value.ptr, name).vType;
        registryUpdate(registryGetArgument(valType, i), arg);

    }

}

void writeFuncCall(FILE * file, CODE_TUPLE tuple) {

    CODE_PARAM_LIST * params = NULL;
    if (tuple.arg2.type)
        params = tuple.arg2.value.ptr;

    if (params && params->amount > 6) make_error(fprintf(stderr, "Maximum argument count exeeded! (%d > 6)\n", params->amount));

    for (int i = 0; params && i < params->amount; ++i) {

        CODE_ARG arg = params->args[i];

        forceValueRegistry(file, arg, registryGetArgument(arg.valType, i), arg.valType);

    }

    fprintf(file, "\tcall %s\n", tuple.arg1.value.name);

    wipeRegistries();

    int retReg = registryGetReturn(symbolTableLookup(symbolTable, tuple.arg1.value.name).vType);
    registryUpdate(retReg, tuple.res);
    addressDescriptorPush(addressDescriptor, tuple.res, (LOCATION){retReg+1, 0, NULL});


}

void writeReturn(FILE * file, CODE_TUPLE tuple) {

    //loadRegistryValue(file, 0, tuple.arg1);
    if (tuple.arg1.type) {
        tuple.arg1.valType = symbolTableLookup(symbolTable, theFuncName).vType;
        forceValueRegistry(file, tuple.arg1, registryGetReturn(tuple.arg1.valType), tuple.arg1.valType);
    }

    fprintf(file, "\tleave\n");
    fprintf(file, "\tret\n\n");

    fprintf(file, ".LFE%d:\n", funcNum);
    fprintf(file, "\t.size %s, .-%s\n", theFuncName, theFuncName);
    funcNum++;

}

void writeCalcOperation(FILE * file, CODE_TUPLE tuple) {

    LOCATION resLoc;
    LOCATION argLoc;
    resLoc.isRef = 0;
    argLoc.isRef = 0;


    VALUE_TYPE resType = getTypeSize(tuple.arg1.valType) > getTypeSize(tuple.arg2.valType) ? tuple.arg1.valType : tuple.arg2.valType;
    resLoc = moveValueToRegistry(file, tuple.arg1, resType);
    argLoc = moveValueToRegistry(file, tuple.arg2, resType);

    addressDescriptorPop(addressDescriptor, tuple.arg1);
    addressDescriptorPop(addressDescriptor, tuple.arg2);

    addressDescriptorPop(addressDescriptor, tuple.res);

    LOCATION dest;
    dest.isRef = 0;

    //wipeRegistries();

    switch (tuple.op) {

        case '+':
            fprintf(file, "\tadd%s %s, %s\n", getSignMoveSuffix(tuple.arg1.valType, tuple.arg1.valType), locationGetName(argLoc), locationGetName(resLoc));
            //regTable[resLoc] = tuple.res;
            registryUpdate(resLoc.regNum-1, tuple.res);
            dest = resLoc;
            break;
        case '-':
            fprintf(file, "\tsub%s %s, %s\n", getSignMoveSuffix(tuple.arg1.valType, tuple.arg1.valType), locationGetName(argLoc), locationGetName(resLoc));
            registryUpdate(resLoc.regNum-1, tuple.res);
            dest = resLoc;
            break;

        case '*':
            if (getTypeInfo(tuple.arg1.valType.baseId).attributes&TA_FP_TYPE) {
                fprintf(file, "\tmulsd %s, %s\n", locationGetName(argLoc), locationGetName(resLoc));
            } else {
                fprintf(file, "\timul %s, %s\n", locationGetName(argLoc), locationGetName(resLoc));
            }
            registryUpdate(resLoc.regNum-1, tuple.res);
            dest = resLoc;
            break;

        case '/':
            if (!getTypeInfo(tuple.arg1.valType.baseId).attributes&TA_FP_TYPE && !getTypeInfo(tuple.arg2.valType.baseId).attributes&TA_FP_TYPE
                                                                              && !getTypeInfo(tuple.arg2.valType.baseId).attributes&TA_FP_TYPE) {
                forceValueRegistry(file, tuple.arg1, 0, resType);
                fprintf(file, "\tcqto\n");
                forceValueRegistry(file, tuple.arg2, 1, resType);
                argLoc.regNum=2;
                fprintf(file, "\tdiv %s\n", locationGetName(argLoc));
                registryUpdate(0, tuple.res);
                resLoc.regNum=1;
                dest = resLoc;
            } else {
                fprintf(file, "\tdivsd %s, %s\n", locationGetName(argLoc), locationGetName(resLoc));
                registryUpdate(resLoc.regNum-1, tuple.res);
                dest = resLoc;
            }
            break;

        case '%':
            //fprintf(file, "\tmov $0, %%ebx\n");
            forceValueRegistry(file, tuple.arg1, 0, resType);
            fprintf(file, "\tcqto\n");
            forceValueRegistry(file, tuple.arg2, 1, resType);
            argLoc.regNum=2;
            fprintf(file, "\tdiv %s\n", locationGetName(argLoc));
            registryUpdate(0, tuple.res);
            resLoc.regNum=3;
            dest = resLoc;
            break;

    }

    addressDescriptorPush(addressDescriptor, tuple.res, dest);

}

void writeAffection(FILE * file, CODE_TUPLE tuple) {

    if (tuple.res.type != ARG_SYMBOL && tuple.res.type != ARG_REF && tuple.res.type != ARG_REG)
        make_error(fprintf(stderr, "Affection needs to affect to symbol not %d\n", tuple.res.type));

    LOCATION origin;
    LOCATION dest;
    LOCATION tmp;
    tmp.isRef = 0;

    CODE_ARG tmpArg;
    tmpArg.type = ARG_REG;
    tmpArg.valType = tuple.res.valType;
    tmpArg.value.num = 123;

    origin = addressDescriptorPop(addressDescriptor, tuple.arg1);

    if (tuple.res.type == ARG_REF) {

        dest = addressDescriptorGet(addressDescriptor, tuple.res);
        dest.isRef = 1;
        //dest.regNum+=1;

    } else if(tuple.res.type == ARG_REG) {
        dest.regNum = getReg(tuple.res.valType, tuple.res)+1;
        dest.isRef = 0;
    } else {

        dest.regNum = 0;
        dest.isRef = 0;
        dest.varOffset = symbolTableLookup(symbolTable, tuple.res.value.name).memOffset;

    }

    switch (tuple.arg1.type) {

        case ARG_SYMBOL:
            fprintf(file, "\t//Affecting to registry\n");
            fprintf(file, "\tmov%s %s, %s\n", getSignMoveSuffix(tuple.arg1.valType, tuple.res.valType), locationGetName(origin), locationGetName(dest));
            addressDescriptorPush(addressDescriptor, tuple.res, dest);
            break;

        case ARG_REG:

            ///TODO UNDEFINED TYPE for tuple.arg1!!!
            if (tuple.arg1.valType.baseId != tuple.res.valType.baseId && dest.varOffset)
                fprintf(file, "\tmov %s, %s\n", locationGetName(origin), locationGetName(dest));
            else
                fprintf(file, "\tmov%s %s, %s\n", getSignMoveSuffix(tuple.arg1.valType, tuple.res.valType), locationGetName(origin), locationGetName(dest));
            break;

        case ARG_CONSTANT:
            moveConstantToLocation(file, tuple.arg1, dest, tuple.res.valType);
            //fprintf(file, "\tmovl $%d, %s\n", tuple.arg1.value.num, locationGetName(dest));
            break;

        case ARG_REF:
            tmp.regNum = getReg(tuple.res.valType, tmpArg)+1;
            origin.isRef = 1;
            origin.regNum = getReg(tuple.res.valType, tuple.arg1)+1;
            fprintf(file, "\tmovq %s, %s\n", locationGetName(origin), locationGetName(tmp));
            fprintf(file, "\tmovq %s, %s\n", locationGetName(tmp), locationGetName(dest));
            break;


    }

}

void writeComparison(FILE * file, CODE_TUPLE tuple) {

    LOCATION arg2Loc;
    LOCATION arg1Loc;

    VALUE_TYPE resType = getTypeSize(tuple.arg1.valType) > getTypeSize(tuple.arg2.valType) ? tuple.arg1.valType : tuple.arg2.valType;

    arg2Loc = moveValueToRegistry(file, tuple.arg2, resType);
    arg1Loc = moveValueToRegistry(file, tuple.arg1, resType);

    fprintf(file, "\tcmp %s, %s\n", locationGetName(arg2Loc), locationGetName(arg1Loc));

    addressDescriptorPop(addressDescriptor, tuple.arg2);
    addressDescriptorPop(addressDescriptor, tuple.arg1);
    wipeRegistries();

}

void writeLabel(FILE * file, CODE_TUPLE tuple) {

    fprintf(file, "%s:\n", tuple.arg1.value.name);

}

void writeJump(FILE * file, CODE_TUPLE tuple) {
    fprintf(file, "\tjmp %s\n", tuple.arg1.value.name);
}

void writeConditionalJump(FILE * file, CODE_TUPLE tuple) {

    switch (tuple.arg2.value.num) {

        case '<':
            fprintf(file, "\tjl %s\n", tuple.arg1.value.name);
            break;

        case '>':
            fprintf(file, "\tjg %s\n", tuple.arg1.value.name);
            break;

        case AST_COMP_EQ:
            fprintf(file, "\tje %s\n", tuple.arg1.value.name);
            break;

        case AST_COMP_NEQ:
            fprintf(file, "\tjne %s\n", tuple.arg1.value.name);
            break;

        case AST_COMP_LEQ:
            fprintf(file, "\tjle %s\n", tuple.arg1.value.name);
            break;

        case AST_COMP_GEQ:
            fprintf(file, "\tjge %s\n", tuple.arg1.value.name);
            break;

        default:
            make_error(fprintf(stderr, "Can not convert condition to jump %ld\n", tuple.arg2.value.num));

    }

}

void writeGetAddress(FILE * file, CODE_TUPLE tuple) {

    LOCATION loc;
    loc.regNum = getReg(tuple.arg1.valType, tuple.res) + 1;
    loc.isRef = 0;
    registryUpdate(loc.regNum-1, tuple.res);
    addressDescriptorPush(addressDescriptor, tuple.res, loc);
    fprintf(file, "\tlea %s, %s\n", locationGetName(addressDescriptorGet(addressDescriptor, tuple.arg1)), locationGetName(loc));

}

void writeTupleToFile(FILE * file, CODE_TUPLE tuple) {

    switch (tuple.op) {

        case OP_CHANGE_SYM_TABLE:
            writeFuncHeader(file, tuple);
            break;

        case OP_RETURN:
            writeReturn(file, tuple);
            break;

        case OP_CALL:
            writeFuncCall(file, tuple);
            break;

        case '+':
        case '-':
        case '*':
        case '/':
            writeCalcOperation(file, tuple);
            break;

        case '&':
            writeGetAddress(file, tuple);
            break;

        case OP_COMPARE:
            writeComparison(file, tuple);
            break;

        case OP_LABEL:
            writeLabel(file, tuple);
            break;

        case OP_JUMP:
            writeJump(file, tuple);
            break;

        case OP_JUMP_COND:
            writeConditionalJump(file, tuple);
            break;

        case '=':
            writeAffection(file, tuple);
            break;

        default:
            break;

    }

}

void writeAssemblyToFile(FILE * file, CODE_STREAM * stream, SYMBOL_TABLE * globalSymbolTable) {

    symbolTable = globalSymbolTable;

    addressDescriptor = createAddressDescriptor(128);

    int symCount = 0;
    SYMBOL_INFO * symbols = symbolTableToArray(symbolTable, &symCount);

    fprintf(file, "\t.section .rodata\n");

    for (int i = 0; i < symCount; ++i) {

        if (symbols[i].oType == O_VARIABLE) {
            /// Add static var aloc
        }

    }

    fprintf(file, "\t.text\n");

    for (int i = 0; i < symCount; ++i) {

        if (symbols[i].oType == O_FUNCTION) {
            fprintf(file, "\t.globl %s\n", symbols[i].name);
            fprintf(file, "\t.type %s, @function\n", symbols[i].name);
        }

    }

    //free(symbols);

    codestreamResetRead(stream);
    CODE_TUPLE tuple;
    while ((tuple = codestreamGet(stream)).op) {

        writeTupleToFile(file, tuple);

    }

    fprintf(file, "\n\n\t.section .rodata\n");
    writeConstList(file);
    fprintf(file, "\n");

}

