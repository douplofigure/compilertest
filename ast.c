#include "ast.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "types.h"
#include "hashmap.h"
#include "prettyprint.h"

struct attribute_t {

    ATTRIBUTE_TYPE type;
    VALUE value;
    ATTRIBUTE * next;

};

struct ast_t {

    TOKEN_TYPE type;
    int childCount;
    int maxChildCount;
    AST ** children;
    ATTRIBUTE * attribs;

};

AST * astCreate(TOKEN_TYPE t, int maxChildCount) {

    AST * ast = malloc(sizeof(AST));

    ast->type = t;
    ast->childCount = 0;
    ast->maxChildCount = maxChildCount;
    ast->children = malloc(sizeof(AST*) * maxChildCount);
    ast->attribs = NULL;

    return ast;

}

VALUE getAttributeValue(ATTRIBUTE * attr, ATTRIBUTE_TYPE type) {

    while (attr) {

        if (attr->type == type) return attr->value;

        attr = attr->next;
    }

    return (VALUE) 0;

}

VALUE astGetAttributeValue(AST * ast, ATTRIBUTE_TYPE type) {
    return getAttributeValue(ast->attribs, type);
}

AST * astCreateChildren(TOKEN_TYPE t, int childCount, ...) {

    va_list ap;
    va_start(ap, childCount);

    int cCount = 0;

    AST ** children = malloc(sizeof(AST*) * childCount);

    for (int i = 0; i < childCount; ++i) {

        AST * tmp = va_arg(ap, AST*);

        if (tmp->type == AST_INSTRUCTION_JOIN && t == AST_INSTRUCTION_JOIN) {
            cCount += tmp->childCount;
            children[i] = tmp;
        } else {
            cCount++;
            children[i] = tmp;
        }
        //astAddChild(ast, tmp);


    }
    AST * ast = astCreate(t, cCount);
    for (int i = 0; i < childCount; ++i) {
        if (children[i]->type == AST_INSTRUCTION_JOIN && t == AST_INSTRUCTION_JOIN) {
            for (int j = 0; j < children[i]->childCount; ++j) {
                astAddChild(ast, children[i]->children[j]);
            }
            free(children[i]);
        } else {
            astAddChild(ast, children[i]);
        }
    }

    free(children);

    VALUE v1, v2;

    switch(t) {

    case '+':
    case '-':
    case '*':
    case '/':
        v1 = getAttributeValue(ast->children[0]->attribs, A_VALUE_TYPE);
        v2 = getAttributeValue(ast->children[1]->attribs, A_VALUE_TYPE);
        astAddAttribute(ast, A_VALUE_TYPE, (VALUE) typeGetSmallestCommon(v1.vt, v2.vt));
        break;

    case '<':
    case '>':
    case AST_COMP_EQ:
    case AST_COMP_GEQ:
    case AST_COMP_NEQ:
    case AST_COMP_LEQ:
        v1 = getAttributeValue(ast->children[0]->attribs, A_VALUE_TYPE);
        v2 = getAttributeValue(ast->children[1]->attribs, A_VALUE_TYPE);
        astAddAttribute(ast, A_VALUE_TYPE, (VALUE) typeGetSmallestCommon(v1.vt, v2.vt));
        break;

    case '=':
        v1 = getAttributeValue(ast->children[0]->attribs, A_VALUE_TYPE);
        v2 = getAttributeValue(ast->children[1]->attribs, A_VALUE_TYPE);
        astAddAttribute(ast, A_VALUE_TYPE, (VALUE) typeGetSmallestCommon(v1.vt, v2.vt));
        break;

    case AST_VAR_DECLARATION:
        break;
    default:
        break;

    }

    return ast;

}

void astAddChild(AST * ast, AST * child) {
    if (ast->childCount >= ast->maxChildCount) return;
    ast->children[ast->childCount++] = child;
}

AST ** astGetChildren(AST * ast, int * cCount) {

    *cCount = ast->childCount;
    return ast->children;

}

TOKEN_TYPE astGetType(AST * ast) {
    return ast->type;
}

void astAddAttribute(AST * ast, ATTRIBUTE_TYPE type, VALUE val) {

    ATTRIBUTE * attribute = malloc(sizeof(ATTRIBUTE));
    attribute->type = type;
    attribute->value = val;

    attribute->next = ast->attribs;
    ast->attribs = attribute;

}

void printValue(VALUE_TYPE type, VALUE val) {

    printf("%ld", val.l);

}

void attributePrintString(ATTRIBUTE * attr, int depth) {

    while (attr) {
        for (int i = 0; i < depth; ++i)
            printf("    ");

        printf("<%d, '%s'>\n", attr->type, attr->value.str);

        attr = attr->next;

    }

}

void attributePrint(ATTRIBUTE * attr, int depth) {

    VALUE_TYPE valType;
    VALUE val;
    while (attr) {
        for (int i = 0; i < depth; ++i)
            printf("    ");
        switch(attr->type) {

        case A_VALUE:
            printf("<value, ");
            printValue(valType, attr->value);
            printf(">\n");
            break;

        case A_VALUE_TYPE:
            valType = attr->value.vt;
            printf("<value_type, ");
            printValue(valType, attr->value);
            printf(">\n");
            break;

        case A_SYM_TABLE:
            printf("<symbol_table,\n");
            symbolTablePrint(attr->value.ptr, depth+1);
            for (int i = 0; i < depth; ++i)
                printf("    ");
            printf(">\n");
            break;

        default:
            printf("<%d, %d>\n", attr->type, attr->value.i);

        }

        attr = attr->next;

    }

}

void astPrint(AST * ast, int depth) {
    for (int i = 0; i < depth; ++i)
        printf("    ");

    ATTRIBUTE * attr = ast->attribs;

    switch(ast->type) {

    case AST_INSTRUCTION_JOIN:
        printf("NODE (INSTRUCTION_JOIN): (%d) {\n", ast->childCount);
        break;

    case AST_VAR_DECLARATION:
        printf("NODE (VAR_DECLARATION %d): {\n", ast->type);
        break;

    case AST_FUNC_DECL:
        printf("NODE (FUNC_DECLARATION): {\n");
        break;

    case AST_ID:
        printf("NODE (ID): {\n");
        break;

    case AST_CONST:
        printf("NODE (CONST): {\n");
        break;

    case AST_CALL:
        printf("NODE (CALL) : {\n");
        break;

    case AST_RETURN:
        printf("NODE (RETURN) : {\n");
        break;

    default:
        if (ast->type < 256) printf("NODE ('%c'): (%d) {\n", ast->type, ast->childCount);
        else
            printf("NODE (%d): (%d) {\n", ast->type, ast->childCount);

    }
    attributePrint(ast->attribs, depth+1);
    for (int i = 0; i < ast->childCount; ++i) {
        astPrint(ast->children[i], depth + 1);
    }
    for (int i = 0; i < depth; ++i)
        printf("    ");
    printf("}\n");
}

