#ifndef _AST_H
#define _AST_H

#include "types.h"

typedef enum ast_token_type_e {

    AST_INSTRUCTION_JOIN = 0,
    AST_VAR_DECLARATION = 256,
    AST_FUNC_DECL,
    AST_RETURN,
    AST_CALL,
    AST_ID,
    AST_CONST,
    AST_IF,
    AST_WHILE,
    AST_FOR,
    AST_DEREF,
    AST_COMP_EQ,
    AST_COMP_LEQ,
    AST_COMP_GEQ,
    AST_COMP_NEQ,
    AST_INC_PRE,
    AST_INC_POST,

} TOKEN_TYPE;

typedef enum attribute_type_e {

    A_VALUE_TYPE,
    A_VALUE,
    A_SYM_TABLE,

} ATTRIBUTE_TYPE;

typedef struct ast_t AST;
typedef struct attribute_t ATTRIBUTE;

AST * astCreate(TOKEN_TYPE t, int maxChildCount);
AST * astCreateChildren(TOKEN_TYPE t, int childCount, ...);
AST ** astGetChildren(AST * ast, int * cCount);
TOKEN_TYPE astGetType(AST * ast);
void astAddChild(AST * ast, AST * child);
void astAddAttribute(AST * ast, ATTRIBUTE_TYPE type, VALUE val);
void astPrint(AST * ast, int depth);

VALUE astGetAttributeValue(AST * ast, ATTRIBUTE_TYPE type);

#endif
