#include "set.h"
#include "automaton.h"

#include <stdlib.h>
#include <stdio.h>

typedef struct set_q_entry_t {

    INT_SET * set;
    struct set_q_entry_t * next;

} SET_Q_ENTRY;

typedef struct set_queue_t {

    SET_Q_ENTRY * first;
    SET_Q_ENTRY * last;

} SET_QUEUE;

SET_QUEUE * setQueueCreate() {

    SET_QUEUE * q = malloc(sizeof(SET_QUEUE));
    q->first = NULL;
    q->last = NULL;

    return q;

}

void setQueuePush(SET_QUEUE * q, INT_SET * set) {

    SET_Q_ENTRY * entry = malloc(sizeof(SET_Q_ENTRY));
    entry->next = NULL;
    entry->set = set;

    if (q->last)
        q->last->next = entry;

    q->last = entry;

    if (!q->first)
        q->first = entry;

}

INT_SET * setQueuePop(SET_QUEUE * q) {

    SET_Q_ENTRY * entry = q->first;
    q->first = entry->next;
    if (!q->first) q->last = NULL;

    INT_SET * tmp = entry->set;
    free(entry);
    return tmp;

}

int setQueueContains(SET_QUEUE * q, INT_SET * set) {

    SET_Q_ENTRY * entry = q->first;
    while (entry) {

        if (intSetEqual(entry->set, set))
            return 1;

        entry = entry->next;

    }

    return 0;

}

int setQueueEmpty(SET_QUEUE * q) {
    return !q->first;
}

INT_SET * buildSetFromArray(int * data, int count) {

    INT_SET * set = intSetCreate();

    for (int i = 0; i < count; ++i) {

        intSetInsert(set, data[i]);

    }

    return set;

}

INT_SET * getDestinations(NFA * nfa, INT_SET * states, int input) {

    int tCount;
    NFA_TRANS * transition = nfaGetTransitions(nfa, &tCount);

    INT_SET * dests = intSetCreate();

    for (int i = 0; i < tCount; ++i) {

        if (intSetContains(states, transition[i].originState) && transition[i].input == input)
            dests = intSetMerge(dests, buildSetFromArray(transition[i].destStates, transition[i].destStateCount), 1);

    }

    return dests;

}

void pushDestination(NFA * nfa, INT_SET * states, SET_QUEUE * queue, INT_SET_SET * builtStates) {

    int tCount;
    NFA_TRANS * transition = nfaGetTransitions(nfa, &tCount);

    INT_SET * checkedInputs = intSetCreate();

    for (int i = 0; i < tCount; ++i) {

        if (intSetContains(states, transition[i].originState) && !intSetContains(checkedInputs, transition[i].input)) {

            INT_SET * dests = getDestinations(nfa, states, transition[i].input);

            if (!intSetSetContains(builtStates, dests) && !setQueueContains(queue, dests))
                setQueuePush(queue, dests);
            intSetInsert(checkedInputs, transition[i].input);

        }

    }

    intSetDestroy(checkedInputs);

}

void linkDestinations(NFA * nfa, INT_SET * set, INT_SET_SET * builtStates) {

    int tCount;
    NFA_TRANS * transition = nfaGetTransitions(nfa, &tCount);

    INT_SET * checkedInputs = intSetCreate();

    for (int i = 0; i < tCount; ++i) {

        intSetContains(set, transition[i].originState);
        intSetContains(checkedInputs, transition[i].input);

        if (intSetContains(set, transition[i].originState) && !intSetContains(checkedInputs, transition[i].input)) {

            INT_SET * dests = getDestinations(nfa, set, transition[i].input);

            intSetInsert(checkedInputs, transition[i].input);

            DFA_STATE * state = intSetSetGetData(builtStates, set);
            int isNew = 1;
            for (int j = 0; j < state->transitionCount; ++j)
                if (state->transitions[j].input == transition[i].input)
                    isNew = 0;

            if (!isNew) continue;

            if (!state->transitionCount) {
                state->transitionCount = 1;
                state->transitions = malloc(sizeof(DFA_TRANS));
                state->transitions[0].destState = intSetSetGetData(builtStates, dests);
                state->transitions[0].input = transition[i].input;
            } else {
                state->transitionCount++;
                state->transitions = realloc(state->transitions, sizeof(DFA_TRANS) * state->transitionCount);
                state->transitions[state->transitionCount-1].destState = intSetSetGetData(builtStates, dests);
                state->transitions[state->transitionCount-1].input = transition[i].input;
            }


        }


    }
    intSetDestroy(checkedInputs);

}

int isSetFinal(NFA * nfa, INT_SET * set) {

    int fCount;
    int * finals = nfaGetFinals(nfa, &fCount);

    for (int i = 0; i < fCount; ++i) {

        if (intSetContains(set, finals[i]))
            return 1;

    }

    return 0;

}

DFA_STATE * buildDFA(NFA * nfa) {

    INT_SET_SET * builtStates = intSetSetCreate();

    SET_QUEUE * queue = setQueueCreate();

    INT_SET * startSet = intSetCreate();
    intSetInsert(startSet, nfaGetStart(nfa));

    setQueuePush(queue, startSet);

    while (!setQueueEmpty(queue)) {


        INT_SET * set = setQueuePop(queue);
        DFA_STATE * state = malloc(sizeof(DFA_STATE));
        state->name = set;
        state->transitionCount = 0;
        state->transitions = NULL;
        state->isFinal = isSetFinal(nfa, set);

        pushDestination(nfa, set, queue, builtStates);

        intSetSetInsert(builtStates, set, state);


    }

    int stateCount;
    INT_SET ** states = intSetSetAsArray(builtStates, &stateCount);

    for (int i = 0; i < stateCount; ++i) {

        linkDestinations(nfa, states[i], builtStates);

    }

    FILE * file = fopen("dfa.dot", "w");
    fprintf(file, "digraph \"automaton\"\n{\n  node [fontname = courier, shape = circle, colorscheme = paired6]\n  edge [fontname = courier]\n  rankdir=\"LR\"\n\n");

    for (int i = stateCount-1; i >= 0; --i) {

        dfaWriteFile(intSetSetGetData(builtStates, states[i]), file, i==stateCount-1);

    }

    fprintf(file, "}\n");

    fclose(file);

    DFA_STATE * state = intSetSetGetData(builtStates, startSet);

    intSetSetDestroy(builtStates, 0);

    return state;

}
