#include "automaton.h"

#include <stdlib.h>
#include <stdio.h>

#include "prettyprint.h"

struct nfa_t {

    int stateCount; //States will be 0 to stateCount-1.
    SMART_ARRAY * transitions;
    int startState;
    SMART_ARRAY * endStates;

};

int isEndState(NFA * nfa, int state) {

    int esCount;
    int * endStates = saGetData(nfa->endStates, &esCount);

    for (int i = 0; i < esCount; ++i) {
        if (endStates[i] == state) return 1;
    }
    return 0;
}

int nfaGetStart(NFA * nfa) {
    return nfa->startState;
}

NFA * nfaCreate(int stateCount, SMART_ARRAY * transitions, int startState, SMART_ARRAY * endStates) {

    NFA * nfa = malloc(sizeof(NFA));
    nfa->stateCount = stateCount;
    nfa->transitions = transitions;
    nfa->startState = startState;
    nfa->endStates = endStates;

    return nfa;

}

NFA * nfaMergeAppend(NFA * a1, NFA * a2) {

    int bOffset = a1->stateCount;
    //nfaPrint(a2);

    NFA * res = malloc(sizeof(NFA));
    res->stateCount = a1->stateCount + a2->stateCount;
    res->startState = a1->startState;

    res->endStates = a2->endStates;

    int stCount;
    int * endStates = saGetData(res->endStates, &stCount);

    for (int i = 0; i < stCount; ++i) {

        endStates[i] += bOffset;

    }

    int tCount;
    NFA_TRANS * transitions = saGetData(a2->transitions, &tCount);
    for (int i = 0; i < tCount; ++i) {

        transitions[i].originState += bOffset;
        for (int j = 0; j < transitions[i].destStateCount; ++j) {

            transitions[i].destStates[j] += bOffset;

        }

    }
    //nfaPrint(a2);

    transitions = saGetData(a1->transitions, &tCount);
    for (int i = 0; i < tCount; ++i) {

        for (int j = 0; j < transitions[i].destStateCount; ++j) {

            if (isEndState(a1, transitions[i].destStates[j])) {

                transitions[i].destStates = realloc(transitions[i].destStates, sizeof(int) * (transitions[i].destStateCount+1));
                transitions[i].destStates[transitions[i].destStateCount] = a2->startState + bOffset;

                transitions[i].destStateCount++;

            }

        }

    }

    /// Epsilon in L(a1)
    if (isEndState(a1, a1->startState)) {
        int t2Count;
        NFA_TRANS * trans2 = saGetData(a2->transitions, &t2Count);

        for (int i = 0; i < t2Count; ++i) {

            if (trans2[i].originState == a2->startState+bOffset) {

                int added = 0;
                for (int j = 0; j < tCount; ++j) {
                    if (transitions[j].originState == a1->startState && transitions[j].input == trans2[i].input) {
                        added = 1;
                        transitions[j].destStates = realloc(transitions[j].destStates, sizeof(int) * (transitions[j].destStateCount+trans2[i].destStateCount));
                        for (int k = 0; k < trans2[i].destStateCount; ++k) {
                            transitions[j].destStates[transitions[j].destStateCount+k] = trans2[i].destStates[k];
                        }
                        transitions[j].destStateCount+=trans2[i].destStateCount;
                    }

                }

                if (!added) {
                    NFA_TRANS trans;
                    trans.originState = a1->startState;
                    trans.destStates = malloc(sizeof(int) * trans2[i].destStateCount);
                    for (int j = 0; j < trans2[i].destStateCount; ++j) {

                        trans.destStates[j] = trans2[i].destStates[j];

                    }
                    trans.destStateCount = trans2[i].destStateCount;
                    trans.input = trans2[i].input;
                    saAppend(a1->transitions, &trans);

                }

            }

        }

    }

    saConcat(a1->transitions, a2->transitions);

    res->transitions = a1->transitions;

    free(a1);
    free(a2);

    return res;


}

NFA * nfaMergeOr(NFA * a1, NFA * a2) {

    NFA * res = malloc(sizeof(NFA));
    res->stateCount = a1->stateCount + a2->stateCount + 1;
    res->startState = 0;

    int bOffset = a1->stateCount;

    int stCount;
    int * endStates = saGetData(a2->endStates, &stCount);

    for (int i = 0; i < stCount; ++i) {

        endStates[i] += bOffset+1;

    }

    endStates = saGetData(a1->endStates, &stCount);

    for (int i = 0; i < stCount; ++i) {

        endStates[i] += 1;

    }

    res->endStates = a1->endStates;
    saConcat(res->endStates, a2->endStates);

    SMART_ARRAY * newTrans = saCreate(sizeof(NFA_TRANS), 0);

    int tCount;
    NFA_TRANS * transitions = saGetData(a2->transitions, &tCount);
    for (int i = 0; i < tCount; ++i) {

        transitions[i].originState += bOffset + 1;
        for (int j = 0; j < transitions[i].destStateCount; ++j) {

            transitions[i].destStates[j] += bOffset + 1;

        }

        if (transitions[i].originState - (bOffset+1) == a2->startState) {

            NFA_TRANS trans;
            trans.originState = 0;
            trans.destStateCount = transitions[i].destStateCount;
            trans.destStates = malloc(sizeof(int) * trans.destStateCount);
            for (int k = 0; k < trans.destStateCount; ++k) {
                trans.destStates[k] = transitions[i].destStates[k];
            }
            trans.input = transitions[i].input;
            saAppend(newTrans, &trans);

        }

    }

    transitions = saGetData(a1->transitions, &tCount);
    for (int i = 0; i < tCount; ++i) {

        transitions[i].originState += 1;

        for (int j = 0; j < transitions[i].destStateCount; ++j) {

            transitions[i].destStates[j] += 1;

        }

        if (transitions[i].originState-1 == a2->startState) {
            NFA_TRANS trans;
            trans.originState = 0;
            trans.destStateCount = transitions[i].destStateCount;
            trans.destStates = malloc(sizeof(int) * trans.destStateCount);
            for (int k = 0; k < trans.destStateCount; ++k) {
                trans.destStates[k] = transitions[i].destStates[k];
            }
            trans.input = transitions[i].input;
            saAppend(newTrans, &trans);

        }

    }

    //printf("adding transitions from a1\n");
    //saConcat(newTrans, a1->transitions);
    printf("adding transitions from a2\n");
    saConcat(a1->transitions, a2->transitions);
    printf("Concat with new Transitions");
    saConcat(newTrans, a1->transitions);

    res->transitions = newTrans;

    free(a1);
    free(a2);

    return res;


}

NFA * nfaOperatorStar(NFA * nfa) {

    int tCount;
    NFA_TRANS * transitions = saGetData(nfa->transitions, &tCount);

    for (int i = 0; i < tCount; ++i) {

        int dCount = transitions[i].destStateCount;

        for (int j = 0; j < dCount; ++j) {

            if (isEndState(nfa, transitions[i].destStates[j])) {

                transitions[i].destStates = realloc(transitions[i].destStates, sizeof(int) * (transitions[i].destStateCount+1));

                transitions[i].destStates[transitions[i].destStateCount] = nfa->startState;

                transitions[i].destStateCount++;

            }

        }

    }

    saAppend(nfa->endStates, &nfa->startState);

    return nfa;

}

NFA * nfaCreateSingleChar(int c) {

    SMART_ARRAY * transitions = saCreate(sizeof(NFA_TRANS), 1);
    NFA_TRANS trans;
    trans.destStateCount = 1;
    trans.input = c;
    trans.destStates = malloc(sizeof(int));
    trans.destStates[0] = 1;
    trans.originState = 0;

    saPutData(transitions, 0, &trans);
    //saAppend(transitions, &trans);

    SMART_ARRAY * endStates = saCreate(sizeof(int), 1);
    int es = 1;
    saPutData(endStates, 0, &es);

    NFA * nfa = nfaCreate(2, transitions, 0, endStates);

    return nfa;

}

NFA * nfaCreateCharRange(int start, int end) {

    if (start > end)
        make_error(fprintf(stderr, "Can not create range with start > end\n"));
    if (start == end)
        return nfaCreateSingleChar(start);

    SMART_ARRAY * transitions = saCreate(sizeof(NFA_TRANS), end-start+1);
    for (int i = start; i <= end; ++i) {
        NFA_TRANS trans;
        trans.destStateCount = 1;
        trans.input = i;
        trans.destStates = malloc(sizeof(int));
        trans.destStates[0] = 1;
        trans.originState = 0;

        saPutData(transitions, i-start, &trans);
    }

    SMART_ARRAY * endStates = saCreate(sizeof(int), 1);
    int es = 1;
    saPutData(endStates, 0, &es);

    NFA * nfa = nfaCreate(2, transitions, 0, endStates);

    return nfa;

}

void nfaPrint(NFA * nfa) {

    printf("NFA at %p\n", nfa);
    printf("states = [0..%d]\n", nfa->stateCount-1);
    printf("startState = %d\n", nfa->startState);

    int tmp;
    int * endStates = saGetData(nfa->endStates, &tmp);
    printf("endStates = {");
    for (int i = 0; i < tmp; ++i)
        printf("%d, ", endStates[i]);
    printf("}\n");

    NFA_TRANS * transitions = saGetData(nfa->transitions, &tmp);
    printf("transitions = {\n");
    for (int i = 0; i < tmp; ++i) {
        for (int j = 0; j < transitions[i].destStateCount; ++j){
            printf("\t(%d, '%c', %d)\n", transitions[i].originState, transitions[i].input, transitions[i].destStates[j]);
        }
    }
    printf("}\n");

}

void nfaRemoveState(NFA * nfa, int state) {

    if (state == nfa->startState)
        make_error(fprintf(stderr, "Can not remove start state!\n"));

    if (isEndState(nfa, state))
        make_error(fprintf(stderr, "Can not remove accepting state!\n"));

    int tCount;
    NFA_TRANS * transitions = saGetData(nfa->transitions, &tCount);

    for (int i = 0; i < tCount; ++i) {

        if (transitions[i].originState == state) {
            saRemove(nfa->transitions, i--);
            tCount--;
            continue;
        }
        if (transitions[i].originState > state) transitions[i].originState--;
        for (int j = 0; j < transitions[i].destStateCount; ++j) {

            if (transitions[i].destStates[j] == state) {

                transitions[i].destStates[j] = transitions[i].destStates[--transitions[i].destStateCount];

            }
            if (transitions[i].destStates[j] > state) transitions[i].destStates[j]--;

        }
    }

    int eCount;
    int * endStates = saGetData(nfa->endStates, &eCount);
    for (int i = 0; i < eCount; ++i) {
        if (endStates[i] > state) endStates[i]--;
    }

    nfa->stateCount--;

}

int nfaGetEntryGrade(NFA * nfa, int state) {

    int count = 0;

    int tCount;
    NFA_TRANS * transitions = saGetData(nfa->transitions, &tCount);

    for (int i = 0; i < tCount; ++i) {
        for (int j = 0; j < transitions[i].destStateCount; ++j) {
            if (transitions[i].destStates[j] == state) count++;
        }
    }

    return count;

}

int nfaGetExitGrade(NFA * nfa, int state) {

    int count = 0;

    int tCount;
    NFA_TRANS * transitions = saGetData(nfa->transitions, &tCount);

    for (int i = 0; i < tCount; ++i) {
        if (transitions[i].originState == state) count += transitions[i].destStateCount;
    }

    return count;

}

void nfaSimplyfy(NFA * nfa) {

    for (int i = 0; i < nfa->stateCount; ++i) {

        if (isEndState(nfa, i)) continue;
        if (nfaGetEntryGrade(nfa, i) && nfaGetExitGrade(nfa, i)) continue;

        if (i == nfa->startState) continue;

        nfaRemoveState(nfa, i);
        --i;

    }

    int tCount;
    NFA_TRANS * transitions = saGetData(nfa->transitions, &tCount);

    for (int i = 0; i < tCount; ++i) {

        for (int j = 0; j < transitions[i].destStateCount; ++j) {
            for (int k = j+1; k < transitions[i].destStateCount; ++k) {

                if (transitions[i].destStates[j] == transitions[i].destStates[k]) {
                    transitions[i].destStates[k] = transitions[i].destStates[transitions[i].destStateCount-1];
                    transitions[i].destStateCount--;

                }

            }
        }

    }

}

void nfaSaveGraph(NFA * nfa, char * fname) {

    FILE * file = fopen(fname, "w");

    fprintf(file, "digraph \"automaton\"\n{\n  node [fontname = courier, shape = circle, colorscheme = paired6]\n  edge [fontname = courier]\n  rankdir=\"LR\"\n\n");


    for (int i = 0; i < nfa->stateCount; ++i) {
        fprintf(file, "  %d [label=\"%d\"%s%s]", i, i, isEndState(nfa, i) ? " shape=doublecircle" : "", i==nfa->startState ? " style=filled" : "");
    }

    int tmp;
    NFA_TRANS * transitions = saGetData(nfa->transitions, &tmp);
    for (int i = 0; i < tmp; ++i) {
        for (int j = 0; j < transitions[i].destStateCount; ++j){
            fprintf(file, "  %d -> %d [label=\"%c\"]\n", transitions[i].originState, transitions[i].destStates[j], transitions[i].input);
        }
    }

    fprintf(file, "}\n");

    fclose(file);

}

NFA_TRANS * nfaGetTransitions(NFA * nfa, int * count) {
    return saGetData(nfa->transitions, count);
}

int * nfaGetFinals(NFA * nfa, int * count) {
    return saGetData(nfa->endStates, count);
}

void dfaWriteFile(DFA_STATE * dfa, FILE * file, int asStart) {

    fprintf(file, "  %ld [label=\"", (long)dfa);
    intSetPrintFile(dfa->name, file);
    fprintf(file, "\"%s%s]\n", dfa->isFinal ? " shape=doublecircle" : "", asStart ? " style=filled" : "");

    for (int i = 0; i < dfa->transitionCount; ++i) {

        if (dfa->transitions[i].input)
            fprintf(file, "  %ld -> %ld [label=\"%c\"]\n", (long)dfa, (long)dfa->transitions[i].destState, dfa->transitions[i].input);


    }

}

DFA_STATE * getDestState(DFA_STATE * state, int input) {

    for (int i = 0; i < state->transitionCount; ++i) {

        if (state->transitions[i].input == input)
            return state->transitions[i].destState;

    }

    return NULL;

}

int dfaMatch(DFA_STATE * dfa, char * str) {

    while (*str) {


        dfa = getDestState(dfa, *str);
        if (!dfa) return 0;

        str++;
    }

    return dfa->isFinal;

}

