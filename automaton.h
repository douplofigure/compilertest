#ifndef AUTOMATON_H_INCLUDED
#define AUTOMATON_H_INCLUDED

#include "smartarray.h"
#include "set.h"

typedef struct nfa_trans_t {

    int originState;
    int input;
    int destStateCount;
    int * destStates;

} NFA_TRANS;

typedef struct dfa_state_t DFA_STATE;
typedef struct dfa_trans_t {

    int input;
    DFA_STATE * destState;

} DFA_TRANS;

struct dfa_state_t {

    INT_SET * name;
    int isFinal;
    ///If transition is ommited it will go to an/the error state.
    int transitionCount;
    DFA_TRANS * transitions;

};

typedef struct nfa_t NFA;

void nfaPrint(NFA * nfa);
NFA * nfaCreateSingleChar(int c);
NFA * nfaCreateCharRange(int start, int end);
NFA * nfaMergeOr(NFA * a1, NFA * a2);
NFA * nfaMergeAppend(NFA * a1, NFA * a2);
NFA * nfaCreate(int stateCount, SMART_ARRAY * transitions, int startState, SMART_ARRAY * endStates);
NFA * nfaOperatorStar(NFA * nfa);
void nfaSaveGraph(NFA * nfa, char * fname);
void nfaRemoveState(NFA * nfa, int state);int nfaGetEntryGrade(NFA * nfa, int state);
int nfaGetExitGrade(NFA * nfa, int state);

int nfaGetStart(NFA * nfa);

NFA_TRANS * nfaGetTransitions(NFA * nfa, int * count);
int * nfaGetFinals(NFA * nfa, int * count);

void dfaWriteFile(DFA_STATE * dfa, FILE * file, int asStart);

DFA_STATE * buildDFA(NFA * nfa);
NFA * buildNFA(char * str);
void nfaSimplyfy(NFA * nfa);

int dfaMatch(DFA_STATE * dfa, char * str);

#endif // AUTOMATON_H_INCLUDED
