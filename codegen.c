#include "codegen.h"

#include <stdlib.h>
#include <stdio.h>

#include "prettyprint.h"
#include "types.h"
#include "hashmap.h"

static unsigned long tmpCount;

CODE_TUPLE utilPopulateStream(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction);


CODE_ARG utilCreateArgFromConst(AST * ast, VALUE_TYPE destType) {

    CODE_ARG arg;
    arg.type = ARG_CONSTANT;

    VALUE_TYPE t = astGetAttributeValue(ast, A_VALUE_TYPE).vt;
    arg.valType = destType;

    arg.value.l = 0;
    arg.value.num = 0;

    arg.value.l = typeConvert(destType, t, astGetAttributeValue(ast, A_VALUE)).l;

    return arg;

}

CODE_ARG utilCreateArgFromId(AST * ast, SYMBOL_TABLE * symbolTable) {

    CODE_ARG arg;
    arg.type = ARG_SYMBOL;
    arg.value.name = astGetAttributeValue(ast, A_VALUE).str;
    arg.valType = symbolTableLookup(symbolTable, arg.value.name).vType;

    return arg;

}

CODE_TUPLE utilCreateTupleOperation(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    ///Error checking
    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    if (childCount != 2)
        make_error(fprintf(stderr, "Not enougth arguments for operator '%c'", astGetType(ast)));

    CODE_TUPLE code;

    code.op = astGetType(ast);

    TOKEN_TYPE cType = astGetType(children[0]);

    if (cType == AST_CONST) {
        code.arg1 = utilCreateArgFromConst(children[0], astGetAttributeValue(ast, A_VALUE_TYPE).vt);
    } else if (cType == AST_ID) {
        code.arg1 = utilCreateArgFromId(children[0], symbolTable);
    } else {
        code.arg1 = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
    }


    cType = astGetType(children[1]);

    if (cType == AST_CONST) {
        code.arg2 = utilCreateArgFromConst(children[1], astGetAttributeValue(ast, A_VALUE_TYPE).vt);
    } else if (cType == AST_ID) {
        code.arg2 = utilCreateArgFromId(children[1], symbolTable);
    } else {
        code.arg2 = utilPopulateStream(children[1], stream, symbolTable, currentFunction).res;
    }


    code.res.type = ARG_REG;
    code.res.value.num = tmpCount++;
    code.res.valType = typeGetSmallestCommon(code.arg1.valType, code.arg2.valType);

    return code;

}

CODE_TUPLE utilCreateTupleAffection(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    ///Error checking
    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    if (childCount != 2)
        make_error(fprintf(stderr, "Not enougth arguments for operator '%c'", astGetType(ast)));

    CODE_TUPLE code;

    code.op = astGetType(ast);

    TOKEN_TYPE cType = astGetType(children[0]);

    if (cType == AST_CONST) {
        make_error(fprintf(stderr, "Cannot assign to constant\n"));
    } else if (cType == AST_ID) {
        code.res = utilCreateArgFromId(children[0], symbolTable);
    } else if (cType == AST_DEREF) {
        code.res = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
    } else {
        code.res = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
    }

    code.arg2.type = ARG_NONE;

    cType = astGetType(children[1]);

    if (cType == AST_CONST) {
        code.arg1.valType = code.res.valType;
        code.arg1 = utilCreateArgFromConst(children[1], code.res.valType);
    } else if (cType == AST_ID) {
        code.arg1 = utilCreateArgFromId(children[1], symbolTable);
        if (!code.res.valType.ptrLvl && code.res.valType.baseId == getTypeId("void"))
            make_error(fprintf(stderr, "Can not assign from void.\n"));
    } else {
        code.arg1 = utilPopulateStream(children[1], stream, symbolTable,currentFunction).res;
        if (!code.res.valType.ptrLvl && code.res.valType.baseId == getTypeId("void"))
            make_error(fprintf(stderr, "Can not assign from void.\n"));
    }

    return code;

}

CODE_TUPLE utilCreateTupleReturn(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    CODE_TUPLE tuple;

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    tuple.op = OP_RETURN;

    TOKEN_TYPE cType = astGetType(children[0]);

    if (cType == AST_CONST) {
        tuple.arg1 = utilCreateArgFromConst(children[0], symbolTableLookup(symbolTable, currentFunction).vType);
    } else if (cType == AST_ID) {
        tuple.arg1 = utilCreateArgFromId(children[0], symbolTable);
    } else {
        tuple.arg1 = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
    }

    tuple.arg1.valType = symbolTableLookup(symbolTable, currentFunction).vType;

    tuple.arg2.type = ARG_NONE;
    tuple.res.type = ARG_NONE;

    return tuple;

}

void utilPopulateArguments(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);


    for (int i = 0; i < childCount; ++i) {

        TOKEN_TYPE cType = astGetType(children[1]);
        CODE_TUPLE code;
        code.op = '=';

        code.res.type = ARG_REG;
        code.res.value.num = i;

        code.arg2.type = ARG_NONE;

        if (cType == AST_CONST) {
            code.arg1 = utilCreateArgFromConst(children[1], astGetAttributeValue(ast, A_VALUE_TYPE).vt);
        } else if (cType == AST_ID) {
            code.arg1 = utilCreateArgFromId(children[1], symbolTable);
        } else {
            code.arg1 = utilPopulateStream(children[1], stream, symbolTable, currentFunction).res;
        }

        codestreamAppend(stream, code);

    }


}

CODE_PARAM_LIST * paramListCreate(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    if (!childCount) {
        return NULL;
    }

    CODE_PARAM_LIST * ls = malloc(sizeof(CODE_PARAM_LIST));
    ls->amount = childCount;
    ls->args = malloc(sizeof(CODE_ARG) * ls->amount);

    for (int i = 0; i < childCount; ++i) {

        TOKEN_TYPE cType = astGetType(children[i]);

        VALUE_TYPE paramType = astGetAttributeValue(children[i], A_VALUE_TYPE).vt;

        if (cType == AST_CONST) {
            ls->args[i] = utilCreateArgFromConst(children[i], paramType);
        } else if (cType == AST_ID) {
            ls->args[i] = utilCreateArgFromId(children[i], symbolTable);
        } else {
            ls->args[i] = utilPopulateStream(children[i], stream, symbolTable, currentFunction).res;
        }

    }

    return ls;

}

CODE_TUPLE utilCreateTupleCall(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    CODE_TUPLE tuple;

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    tuple.op = OP_CALL;
    tuple.arg2.type = ARG_NONE;
    tuple.res.type = ARG_REG;
    tuple.res.value.num = tmpCount++;

    TOKEN_TYPE cType = astGetType(children[0]);

    if (cType == AST_CONST) {
        make_error(fprintf(stderr, "Can not call a numerical constant\n"));
    } else if (cType == AST_ID) {
        tuple.arg1 = utilCreateArgFromId(children[0], symbolTable);
    } else {
        tuple.arg1 = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
    }

    tuple.res.valType = symbolTableLookup(symbolTable, tuple.arg1.value.name).vType;

    if (childCount < 2) return tuple; ///No arguments given, not an erro case.

    cType = astGetType(children[1]);

    if (cType != AST_INSTRUCTION_JOIN)
        make_error(fprintf(stderr, "Expected an argument list!\n"));

    tuple.arg2.value.ptr = paramListCreate(children[1], stream, symbolTable, currentFunction);
    if (tuple.arg2.value.ptr)
        tuple.arg2.type = ARG_PARAM_LIST;
    else
        tuple.arg2.type = ARG_NONE;


    return tuple;

}

void utilCreateTupleFunc(AST * ast, CODE_STREAM * stream) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    SYMBOL_TABLE * table = astGetAttributeValue(ast, A_SYM_TABLE).ptr;
    char * name = astGetAttributeValue(children[0], A_VALUE).str;

    CODE_TUPLE tuple;
    tuple.op = OP_CHANGE_SYM_TABLE;
    tuple.arg1.type = ARG_SYM_TABLE;
    tuple.arg1.value.ptr = table;

    tuple.arg2.type = ARG_SYMBOL;
    tuple.arg2.value.name = name;

    tuple.res.type = ARG_NONE;
    tuple.res.value.ptr = children[1];

    codestreamAppend(stream, tuple);

    utilPopulateStream(children[2], stream, table, name);

    CODE_TUPLE lastTuple = codestreamGetLast(stream);
    if (!lastTuple.op == OP_RETURN) {

        CODE_TUPLE nTuple;
        nTuple.op = OP_RETURN;
        nTuple.arg1.type = ARG_NONE;
        nTuple.arg2.type = ARG_SYMBOL;

        codestreamAppend(stream, nTuple);


    } else {

        lastTuple.arg2.type = ARG_SYMBOL;

        codestreamUpdateLast(stream, lastTuple);

    }

}

int invertCondition(int condition) {

    switch (condition) {

        case '<':
            return AST_COMP_GEQ;
        case '>':
            return AST_COMP_LEQ;

        case AST_COMP_EQ:
            return AST_COMP_NEQ;

        case AST_COMP_GEQ:
            return '<';

        case AST_COMP_LEQ:
            return '>';

        case AST_COMP_NEQ:
            return AST_COMP_EQ;

        default:
            if (condition < 256)
                make_error(fprintf(stderr, "Unknow comparison sign '%c'\n", condition))
            else
                make_error(fprintf(stderr, "Unknow comparison sign %d\n", condition))
        break;

    }

    return 0;

}

void utilCreateTupleIf(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    static unsigned int ifCount;

    CODE_TUPLE tuple;

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    utilPopulateStream(children[0], stream, symbolTable, currentFunction);

    tuple.op = OP_JUMP_COND;
    tuple.arg1.type = ARG_LABEL;
    tuple.arg1.value.name = malloc(sizeof(char) * 13);
    sprintf(tuple.arg1.value.name, ".if%d", ifCount++);

    tuple.arg2.type = ARG_CONDITION;
    tuple.arg2.value.num = invertCondition(astGetType(children[0]));

    tuple.res.type = ARG_NONE;

    codestreamAppend(stream, tuple);

    utilPopulateStream(children[1], stream, symbolTable, currentFunction);

    CODE_TUPLE dest;

    dest.op = OP_LABEL;

    dest.arg1.type = ARG_LABEL;
    dest.arg1.value.name = tuple.arg1.value.name;

    dest.arg2.type = ARG_NONE;
    dest.res.type = ARG_NONE;

    codestreamAppend(stream, dest);



}

CODE_TUPLE utilCreateTupleComparison(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    CODE_TUPLE tuple;
    tuple.op = OP_COMPARE;

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    TOKEN_TYPE cType = astGetType(children[0]);

    if (cType == AST_CONST) {
        tuple.arg1 = utilCreateArgFromConst(children[0], astGetAttributeValue(ast, A_VALUE_TYPE).vt);
    } else if (cType == AST_ID) {
        tuple.arg1 = utilCreateArgFromId(children[0], symbolTable);
    } else {
        tuple.arg1 = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
    }


    cType = astGetType(children[1]);

    if (cType == AST_CONST) {
        tuple.arg2 = utilCreateArgFromConst(children[1], astGetAttributeValue(ast, A_VALUE_TYPE).vt);
    } else if (cType == AST_ID) {
        tuple.arg2 = utilCreateArgFromId(children[1], symbolTable);
    } else {
        tuple.arg2 = utilPopulateStream(children[1], stream, symbolTable, currentFunction).res;
    }


    tuple.res.type = ARG_NONE;
    tuple.res.value.num = 0;

    return tuple;

}

CODE_TUPLE utilCreateTupleWhile(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    static unsigned int whileCount;

    CODE_TUPLE tuple;

    char * startName = malloc(sizeof(char) * 14);
    sprintf(startName, ".wls%d", whileCount);

    char * endName = malloc(sizeof(char) * 14);
    sprintf(endName, ".wle%d", whileCount);

    whileCount++;

    CODE_TUPLE start;

    start.op = OP_LABEL;

    start.arg1.type = ARG_LABEL;
    start.arg1.value.name = startName;

    start.arg2.type = ARG_NONE;
    start.res.type = ARG_NONE;

    codestreamAppend(stream, start);

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    utilPopulateStream(children[0], stream, symbolTable, currentFunction);

    tuple.op = OP_JUMP_COND;
    tuple.arg1.type = ARG_LABEL;
    tuple.arg1.value.name = endName;

    tuple.arg2.type = ARG_CONDITION;
    tuple.arg2.value.num = invertCondition(astGetType(children[0]));

    tuple.res.type = ARG_NONE;

    codestreamAppend(stream, tuple);

    utilPopulateStream(children[1], stream, symbolTable, currentFunction);

    CODE_TUPLE loop;

    loop.op = OP_JUMP;

    loop.arg1.type = ARG_LABEL;
    loop.arg1.value.name = startName;

    loop.arg2.type = ARG_NONE;
    loop.res.type = ARG_NONE;

    codestreamAppend(stream, loop);

    CODE_TUPLE dest;

    dest.op = OP_LABEL;

    dest.arg1.type = ARG_LABEL;
    dest.arg1.value.name = endName;

    dest.arg2.type = ARG_NONE;
    dest.res.type = ARG_NONE;

    codestreamAppend(stream, dest);

}

CODE_TUPLE utilCreateTupleFor(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    static unsigned int forCount;

    CODE_TUPLE tuple;

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    char * startName = malloc(sizeof(char) * 14);
    sprintf(startName, ".fls%d", forCount);

    char * endName = malloc(sizeof(char) * 14);
    sprintf(endName, ".fle%d", forCount);

    forCount++;

    CODE_TUPLE start;

    utilPopulateStream(children[0], stream, symbolTable, currentFunction);

    start.op = OP_LABEL;

    start.arg1.type = ARG_LABEL;
    start.arg1.value.name = startName;

    start.arg2.type = ARG_NONE;
    start.res.type = ARG_NONE;

    codestreamAppend(stream, start);

    utilPopulateStream(children[1], stream, symbolTable, currentFunction);

    tuple.op = OP_JUMP_COND;
    tuple.arg1.type = ARG_LABEL;
    tuple.arg1.value.name = endName;

    tuple.arg2.type = ARG_CONDITION;
    tuple.arg2.value.num = invertCondition(astGetType(children[1]));

    tuple.res.type = ARG_NONE;

    codestreamAppend(stream, tuple);

    utilPopulateStream(children[3], stream, symbolTable, currentFunction);
    utilPopulateStream(children[2], stream, symbolTable, currentFunction);

    CODE_TUPLE loop;

    loop.op = OP_JUMP;

    loop.arg1.type = ARG_LABEL;
    loop.arg1.value.name = startName;

    loop.arg2.type = ARG_NONE;
    loop.res.type = ARG_NONE;

    codestreamAppend(stream, loop);

    CODE_TUPLE dest;

    dest.op = OP_LABEL;

    dest.arg1.type = ARG_LABEL;
    dest.arg1.value.name = endName;

    dest.arg2.type = ARG_NONE;
    dest.res.type = ARG_NONE;

    codestreamAppend(stream, dest);

}

CODE_TUPLE utilCreateTupleGetLocation(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    printf("Get location\n");
    CODE_TUPLE tuple;
    tuple.op = '&';

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    TOKEN_TYPE cType = astGetType(children[0]);

    if (cType == AST_CONST) {
        make_error(fprintf(stderr, "Can not take address of constant\n"));
    } else if (cType == AST_ID) {
        tuple.arg1 = utilCreateArgFromId(children[0], symbolTable);
    } else {
        //tuple.arg1 = utilPopulateStream(children[0], stream, symbolTable, currentFunction).res;
        make_error(fprintf(stderr, "can not take addres from termporary\n"));
    }

    tuple.arg2.type = ARG_NONE;

    tuple.res.type = ARG_REG;
    tuple.res.value.num = tmpCount++;

    return tuple;

}

CODE_TUPLE utilCreateTupleDeref(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    printf("Creating deref\n");
    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    CODE_TUPLE mult;
    mult.op = '*';
    mult.arg1.type = ARG_CONSTANT;
    mult.arg1.valType.baseId = getTypeId("long");
    mult.arg1.valType.ptrLvl = 0;
    mult.arg1.value.name = malloc(sizeof(char) * 10);

    VALUE_TYPE refType = symbolTableLookup(symbolTable, astGetAttributeValue(children[0], A_VALUE).str).vType;
    refType.ptrLvl--;

    sprintf(mult.arg1.value.name, "%d", getTypeSize(refType));

    if (astGetType(children[1]) != AST_CONST) {
        CODE_TUPLE mvAmount = utilPopulateStream(children[1], stream, symbolTable, currentFunction);
        mult.arg2 = mvAmount.res;
    } else {
        mult.arg2 = utilCreateArgFromConst(children[1], mult.arg1.valType);
    }

    mult.res.type = ARG_REG;
    mult.res.value.num = tmpCount++;
    mult.res.valType = mult.arg1.valType;

    codestreamAppend(stream, mult);

    CODE_TUPLE add;
    add.op = '+';
    add.arg1 = mult.res;
    add.arg2 = utilCreateArgFromId(children[0], symbolTable);

    add.res.type = ARG_REF;
    add.res.valType = symbolTableLookup(symbolTable, astGetAttributeValue(children[0], A_VALUE).str).vType;
    add.res.valType.ptrLvl--;
    add.res.value.num = tmpCount++;

    return add;

}

CODE_TUPLE utilCreateTupleConstant(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    CODE_TUPLE tuple;
    printf("Creating tuple for constant %s\n", astGetAttributeValue(ast, A_VALUE).str);
    tuple.op = OP_NONE;
    tuple.res = utilCreateArgFromConst(ast, astGetAttributeValue(ast, A_VALUE_TYPE).vt);
    tuple.arg1.type = ARG_NONE;
    tuple.arg2.type = ARG_NONE;

    return tuple;

}

CODE_TUPLE utilCreateTupleId(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    CODE_TUPLE tuple;
    printf("Creating tuple for constant %s\n", astGetAttributeValue(ast, A_VALUE).str);
    tuple.op = OP_NONE;

    tuple.res = utilCreateArgFromId(ast, symbolTable);
    tuple.arg1.type = ARG_NONE;
    tuple.arg2.type = ARG_NONE;

    return tuple;

}

CODE_TUPLE utilCreateTuplePostIncrement(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    if (!childCount) return (CODE_TUPLE){0};

    CODE_TUPLE load;
    load.op = '=';
    load.arg1.type = ARG_SYMBOL;
    load.arg1.value.name = astGetAttributeValue(children[0], A_VALUE).str;
    load.arg1.valType = symbolTableLookup(symbolTable, load.arg1.value.name).vType;

    load.arg2.type = ARG_NONE;

    load.res.type = ARG_REG;
    load.res.value.num = tmpCount++;
    load.res.valType = symbolTableLookup(symbolTable, load.arg1.value.name).vType;

    codestreamAppend(stream, load);

    CODE_TUPLE add;
    add.op = '+';
    add.arg1.type = ARG_REG;
    add.arg1.valType = symbolTableLookup(symbolTable, load.arg1.value.name).vType;
    add.arg1.value.num = load.res.value.num;
    add.arg2.type = ARG_CONSTANT;
    add.arg2.value.name = "1";
    add.arg2.valType = load.res.valType;

    add.res.type = ARG_REG;
    add.res.value.num = tmpCount++;
    add.res.valType = add.arg1.valType;

    codestreamAppend(stream, add);

    CODE_TUPLE store;
    store.op = '=';
    store.arg1 = add.res;
    store.arg2.type = ARG_NONE;
    store.res.type = ARG_SYMBOL;
    store.res.valType = symbolTableLookup(symbolTable, load.arg1.value.name).vType;
    store.res.value.name = load.arg1.value.name;

    codestreamAppend(stream, store);

    return load;

}

CODE_TUPLE utilPopulateStream(AST * ast, CODE_STREAM * stream, SYMBOL_TABLE * symbolTable, char * currentFunction) {


    if (!ast) return (CODE_TUPLE) {0};
    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    CODE_TUPLE tuple;
    tuple.op = 0;

    TOKEN_TYPE astType = astGetType(ast);

    switch(astType) {

        case AST_INC_POST:
            tuple = utilCreateTuplePostIncrement(ast, stream, symbolTable, currentFunction);
            break;

        case '*':
        case '+':
        case '-':
        case '/':
            tuple = utilCreateTupleOperation(ast, stream, symbolTable, currentFunction);
            break;

        case '<':
        case '>':
        case AST_COMP_EQ:
        case AST_COMP_NEQ:
        case AST_COMP_LEQ:
        case AST_COMP_GEQ:
            tuple = utilCreateTupleComparison(ast, stream, symbolTable, currentFunction);
            break;

        case '&':
            tuple = utilCreateTupleGetLocation(ast, stream, symbolTable, currentFunction);
            break;

        case '=':
            tuple = utilCreateTupleAffection(ast, stream, symbolTable, currentFunction);
            break;

        case AST_FUNC_DECL:
            utilCreateTupleFunc(ast, stream);
            break;

        case AST_RETURN:
            tuple = utilCreateTupleReturn(ast, stream, symbolTable, currentFunction);
            break;

        case AST_CALL:
            tuple = utilCreateTupleCall(ast, stream, symbolTable, currentFunction);
            break;

        case AST_ID:
            tuple = utilCreateTupleId(ast, stream, symbolTable, currentFunction);
            //make_error(fprintf(stderr, "Found ID\n"));
            break;
        case AST_CONST:
            tuple = utilCreateTupleConstant(ast, stream, symbolTable, currentFunction);
            break;

        case AST_VAR_DECLARATION:
            break;

        case AST_IF:
            utilCreateTupleIf(ast, stream, symbolTable, currentFunction);
            break;

        case AST_WHILE:
            utilCreateTupleWhile(ast, stream, symbolTable, currentFunction);
            break;

        case AST_FOR:
            utilCreateTupleFor(ast, stream, symbolTable, currentFunction);
            break;

        case AST_DEREF:
            tuple = utilCreateTupleDeref(ast, stream, symbolTable, currentFunction);
            break;

        case AST_INSTRUCTION_JOIN:
            for (int i = 0; i < childCount; ++i) {

                utilPopulateStream(children[i], stream, symbolTable, currentFunction);

            }
            break;

        default:
            break;


    }

    if (tuple.op)
        codestreamAppend(stream, tuple);


    return tuple;

}

CODE_STREAM * utilCreateCodeStream(AST * ast, SYMBOL_TABLE * globalSymbolTable) {

    CODE_STREAM * stream = codestreamCreate();

    utilPopulateStream(ast, stream, globalSymbolTable, "");

    return stream;

}

