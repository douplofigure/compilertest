#ifndef CODEGEN_H_INCLUDED
#define CODEGEN_H_INCLUDED

#include "codestream.h"
#include "ast.h"
#include "hashmap.h"

#include <stdio.h>

CODE_STREAM * utilCreateCodeStream(AST * ast, SYMBOL_TABLE * table);

void writeAssemblyToFile(FILE * file, CODE_STREAM * stream, SYMBOL_TABLE * globalSymbolTable);

#endif // CODEGEN_H_INCLUDED
