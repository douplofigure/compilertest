#include "codestream.h"

#include <stdio.h>
#include <stdlib.h>

#include "prettyprint.h"

typedef struct code_stream_element_t {

    struct code_stream_element_t * prev;
    struct code_stream_element_t * next;
    CODE_TUPLE instruction;

} CODE_ELEM;

struct code_stream_t {

    CODE_ELEM * writeHead;
    CODE_ELEM * readHead;

};

CODE_STREAM * codestreamCreate() {

    CODE_STREAM * stream = malloc(sizeof(CODE_STREAM));
    stream->writeHead = NULL;
    stream->readHead = NULL;

}

void codestreamAppend(CODE_STREAM * stream, CODE_TUPLE instruction) {

    CODE_ELEM * codeElem = malloc(sizeof(CODE_ELEM));
    codeElem->prev = stream->writeHead;
    codeElem->next = NULL;
    codeElem->instruction = instruction;

    if (stream->writeHead) stream->writeHead->next = codeElem;

    stream->writeHead = codeElem;

    if (!stream->readHead) {
        stream->readHead = codeElem;
    }


}

CODE_TUPLE codestreamGet(CODE_STREAM * stream) {

    if (!stream->readHead) {
        return (CODE_TUPLE) {0, 0, 0};
    }

    CODE_ELEM * elem = stream->readHead;
    CODE_TUPLE code = elem->instruction;

    stream->readHead = elem->next;

    /// free elem if not needed.

    return code;


}

CODE_TUPLE codestreamGetLast(CODE_STREAM * stream) {

    return stream->writeHead->instruction;

}

void codestreamUpdateLast(CODE_STREAM * stream, CODE_TUPLE tuple) {

    stream->writeHead->instruction = tuple;

}

void codestreamResetRead(CODE_STREAM * stream) {

    if (!stream->readHead) return;

    while (stream->readHead->prev) {
        stream->readHead = stream->readHead->prev;
    }

}

char * getArgTypeName(CODE_ARG arg) {

    char * text = malloc(sizeof(char) * 512);

    switch(arg.type) {

        case ARG_NONE:
            strncpy(text, "None", 64);
            break;

        case ARG_SYMBOL:
            sprintf(text, "Symbol:'%s'", arg.value.name);
            break;

        case ARG_CONSTANT:
            sprintf(text, "Constant: %s", arg.value.name);
            break;

        case ARG_REF:
            sprintf(text, "(reg #%ld)", arg.value.num);
            return text;

        case ARG_REG:
            sprintf(text, "reg #%ld", arg.value.num);
            break;

        case ARG_SYM_TABLE:
            strncpy(text, "Symbol-Table", 512);
            break;

        case ARG_LABEL:
            sprintf(text, "Label:'%s'", arg.value.name);
            break;
        default:
            sprintf(text, "%d", arg.type);
            break;


    }

    return text;

}

void tuplePrint(CODE_TUPLE tuple) {

    char * arg1Name = getArgTypeName(tuple.arg1);
    char * arg2Name = getArgTypeName(tuple.arg2);
    char * resName = getArgTypeName(tuple.res);

    if (tuple.op < 256) {
        printf("<'%c', %s, %s, %s>\n", tuple.op, arg1Name, arg2Name, resName);
        return;
    }
    else {
        switch (tuple.op) {

            case OP_CALL:
                printf("<CALL, %s, %s, %s>\n", arg1Name, arg2Name, resName);
                break;

            default:
                printf("<%d, %s, %s, %s>\n",tuple.op, arg1Name, arg2Name, resName);
                break;

        }

    }

    free(arg1Name);
    free(arg2Name);
    free(resName);


}

void codestreamPrint(CODE_STREAM * stream) {

    codestreamResetRead(stream);

    if (!stream->readHead) return;

    CODE_TUPLE tuple;

    CODE_ELEM * startElem = stream->readHead;

    while (stream->readHead) {

        tuple = stream->readHead->instruction;

        tuplePrint(tuple);

        stream->readHead = stream->readHead->next;

    }

    stream->readHead = startElem;
}
