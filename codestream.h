#ifndef CODESTREAM_H_INCLUDED
#define CODESTREAM_H_INCLUDED

#include "types.h"

typedef struct code_stream_t CODE_STREAM;

typedef enum code_arg_type_e {

    ARG_NONE,
    ARG_SYMBOL,
    ARG_CONSTANT,
    ARG_REG,
    ARG_LABEL,
    ARG_CONDITION,
    ARG_REF,

    ARG_SYM_TABLE,
    ARG_PARAM_LIST,

} ARG_TYPE;

typedef enum code_oparator_e {

    OP_NONE,

    OP_CALL = 256,
    OP_RETURN,
    OP_COMPARE,
    OP_JUMP_COND,
    OP_JUMP,
    OP_LABEL,
    OP_DEREF,

    OP_CHANGE_SYM_TABLE=0xffffffff,

} CODE_OPERATOR;

typedef union {
    char * name;
    void * ptr;
    long num;
    double d;
    long l;
    short s;
    char c;
} ARG_VAL;

typedef struct code_arg_t {

    ARG_TYPE type;
    ARG_VAL value;
    VALUE_TYPE valType;

} CODE_ARG;

typedef struct code_tuple_t {

    CODE_OPERATOR op; ///operator
    CODE_ARG arg1;
    CODE_ARG arg2;
    CODE_ARG res;

} CODE_TUPLE;

typedef struct code_param_list_t {

    CODE_ARG * args;
    int amount;

} CODE_PARAM_LIST;

CODE_STREAM * codestreamCreate();
void codestreamAppend(CODE_STREAM * stream, CODE_TUPLE instruction);
CODE_TUPLE codestreamGet(CODE_STREAM * stream);
void codestreamResetRead(CODE_STREAM * stream);
void codestreamPrint(CODE_STREAM * stream);

CODE_TUPLE codestreamGetLast(CODE_STREAM * stream);
void codestreamUpdateLast(CODE_STREAM * stream, CODE_TUPLE tuple);

#endif // CODESTREAM_H_INCLUDED
