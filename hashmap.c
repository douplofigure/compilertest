#include "hashmap.h"

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <math.h>

#include "prettyprint.h"

typedef struct map_entry_t {

    struct map_entry_t * next;
    SYMBOL_INFO value;
    char * key;

} MAP_ENTRY;


struct hashmap_t {

    int maxEntries;
    int itemCount;
    MAP_ENTRY ** entries;

};

struct symbol_table_t {

    MAP * map;
    SYMBOL_TABLE * parent;

};

int isPrime(int n) {

    for (int i = 2; i < sqrt(n); ++i) {
        if (!(n % i)) return 0;
    }
    return 1;
}

int getNextPrime(int n) {

    for (int i = n; i < 2 * n; ++i) {
        if (isPrime(i)) return i;
    }

}

#define FNV_PRIME 16777619
#define HASH_START 2166136261

unsigned int getHash(char * str) {

    int hash = HASH_START;
    while (*str) {

        hash *= FNV_PRIME;
        hash ^= *(str++);

    }

    return hash;

}

MAP * mapCreate(int minSize) {

    int size = getNextPrime(minSize);

    MAP * varmap = malloc(sizeof(MAP));
    varmap->maxEntries = size;
    varmap->itemCount = 0;
    varmap->entries = malloc(sizeof(MAP_ENTRY *) * size);

    for (int i = 0; i < size; ++i) {
        varmap->entries[i] = 0;
    }

    return varmap;

}

void mapInsert(MAP * map, char * name, SYMBOL_INFO info) {

    unsigned int hash = getHash(name);
    unsigned int pos = hash % map->maxEntries;

    if (map->entries[pos]) {
        MAP_ENTRY * entry = map->entries[pos];
        do {
            if (!strcmp(entry->key, name)) {
                entry->value = info;
                return;
            }
        } while (entry = entry->next);

    }

    MAP_ENTRY * entry = malloc(sizeof(MAP_ENTRY));
    entry->key = name;
    entry->value = info;
    entry->next = map->entries[pos];

    map->entries[pos] = entry;

    map->itemCount++;

}

MAP_ENTRY ** mapToEntryArray(MAP * map , int * count) {

    *count = map->itemCount;

    MAP_ENTRY ** arr = malloc(sizeof(MAP_ENTRY*) * map->itemCount);

    int index = 0;

    for (int i = 0; i < map->maxEntries; ++i) {

        MAP_ENTRY * entry = map->entries[i];
        while (entry) {

            arr[index++] = entry;
            entry = entry->next;

        }

    }

    return arr;

}

SYMBOL_INFO * mapToArray(MAP * map, int * count) {

    *count = map->itemCount;
    SYMBOL_INFO * arr = malloc(sizeof(MAP_ENTRY) * map->itemCount);

    int index = 0;

    for (int i = 0; i < map->maxEntries; ++i) {

        MAP_ENTRY * entry = map->entries[i];
        while (entry) {

            arr[index++] = entry->value;
            entry = entry->next;

        }

    }

    return arr;

}

char ** mapGetNameArray(MAP * map) {

    char ** arr = malloc(sizeof(char *) * map->itemCount);

    int index = 0;

    for (int i = 0; i < map->maxEntries; ++i) {

        MAP_ENTRY * entry = map->entries[i];
        while (entry) {

            arr[index++] = entry->key;
            entry = entry->next;

        }

    }

    return arr;

}

SYMBOL_INFO mapLookup(MAP * map, char * name) {

    unsigned int hash = getHash(name);
    unsigned int pos = hash % map->maxEntries;

    if (map->entries[pos]) {
        MAP_ENTRY * entry = map->entries[pos];
        do {
            if (!strcmp(entry->key, name)) {
                return entry->value;
            }
        } while (entry = entry->next);

    }
    return (SYMBOL_INFO) {0, 0};

}

SYMBOL_TABLE * symbolTableCreate(SYMBOL_TABLE * parent) {

    SYMBOL_TABLE * tab = malloc(sizeof(SYMBOL_TABLE));
    tab->parent = parent;
    tab->map = mapCreate(128);

}

void symbolTableInsert(SYMBOL_TABLE * table, char * name, SYMBOL_INFO info) {

    mapInsert(table->map, name, info);

}

void symbolTableUpdateOffset(SYMBOL_TABLE * table, char * name, int newOffset) {

    SYMBOL_INFO info = mapLookup(table->map, name);
    if (info.oType != O_VARIABLE) {
        make_error(fprintf(stderr, "Error changing offset for symbol '%s'\n", name));
        exit(1);
    }

    info.memOffset = newOffset;

    mapInsert(table->map, name, info);

}

SYMBOL_INFO symbolTableLookup(SYMBOL_TABLE * table, char * name) {

    SYMBOL_INFO info = mapLookup(table->map, name);
    if (!info.oType && table->parent) {
        return symbolTableLookup(table->parent, name);
    }

    return info;

}

SYMBOL_INFO * symbolTableToArray(SYMBOL_TABLE * table, int * sCount) {
    return mapToArray(table->map, sCount);
}

char ** symbolTableGetNameArray(SYMBOL_TABLE * table) {
    return mapGetNameArray(table->map);
}

void symbolTablePrint(SYMBOL_TABLE * table, int depth) {

    int entryCount;
    MAP_ENTRY ** entries = mapToEntryArray(table->map, &entryCount);

    for (int i = 0; i < entryCount; ++i) {
        for (int j = 0; j < depth; ++j) printf("    ");

        if (entries[i]->value.oType == O_VARIABLE) {
            printf("<'%s', var, %d, %d>\n", entries[i]->key, entries[i]->value.vType.baseId, entries[i]->value.memOffset);
            continue;
        }

        printf("<'%s', %d, %d>\n", entries[i]->key, entries[i]->value.oType, entries[i]->value.vType.baseId);

    }

}

SYMBOL_TABLE * symbolTableGetParrent(SYMBOL_TABLE * table) {
    return table->parent;
}
