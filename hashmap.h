#ifndef _VARMAP_H
#define _VARMAP_H

#include "types.h"

typedef struct hashmap_t MAP;

typedef struct symbol_info_t  {
    OBJECT_TYPE oType;
    VALUE_TYPE vType;
    int memOffset;
    char * name;
} SYMBOL_INFO;

typedef struct symbol_table_t SYMBOL_TABLE;

MAP * mapCreate(int maxSize);
void mapInsert(MAP * map, char * key, SYMBOL_INFO info);
SYMBOL_INFO mapLookup(MAP * map, char * key);

SYMBOL_TABLE * symbolTableCreate(SYMBOL_TABLE * parent);
SYMBOL_TABLE * symbolTableGetParrent(SYMBOL_TABLE * table);
void symbolTableInsert(SYMBOL_TABLE * table, char * name, SYMBOL_INFO info);
void symbolTableUpdateOffset(SYMBOL_TABLE * table, char * name, int newOffset);
SYMBOL_INFO symbolTableLookup(SYMBOL_TABLE * table, char * name);
void symbolTablePrint(SYMBOL_TABLE * table, int depth);

SYMBOL_INFO * symbolTableToArray(SYMBOL_TABLE * table, int * sCount);
char ** symbolTableGetNameArray(SYMBOL_TABLE * table);

#endif
