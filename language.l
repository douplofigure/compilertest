
%{
#include "types.h"
#include "y.tab.h"
#include "prettyprint.h"

extern int line;

int includeDepth = 0;
void includeFile();
int closeInclude();

%}
%x include
%%

"#include"		BEGIN(include);

<include>[ \t]*
<include>[^ \t\n]* {includeFile(); BEGIN(INITIAL);}
<<EOF>>			{closeInclude(); if (!YY_CURRENT_BUFFER) yyterminate();}

"char"			{return TYPE;}
"short"			{return TYPE;}
"int"			{return TYPE;}
"long"          {return TYPE;}
"void"			{return TYPE;}

"->"			{return PTR;}

"return"		{return F_RETURN;}
"if"			{return F_IF;}
"else"			{return F_ELSE;}
"while"			{return F_WHILE;}
"for"			{return F_FOR;}
"=="			{return CMP_EQ;}
"!="			{return CMP_NEQ;}
"<="			{return CMP_LEQ;}
">="			{return CMP_GEQ;}

[\+\*\/\(\)\-=\,\{\}\;<>\[\]]  {return *yytext;}

[0-9]+			{return INT;}
[0-9]*\.[0-9]+  {return FLOAT;}
[0-9]+\.[0-9]*  {return FLOAT;}

[a-zA-Z][a-zA-Z0-9]*	{return NAME;}

[ \t]			 {}
"\n"			{line++;}

%%

void includeFile() {

	FILE * file = fopen(yytext, "r");
	
	if (!file) make_error(fprintf(stderr, "No such file or directory '%s'\n", yytext));

	YY_BUFFER_STATE state = yy_create_buffer(file, YY_BUF_SIZE);
	yypush_buffer_state(state);

}

int closeInclude() {
	yypop_buffer_state();
}

