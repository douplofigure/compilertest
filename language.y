%{
#include "types.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ast.h"
#include "hashmap.h"
#include "prettyprint.h"

extern int yylex();
extern char * yytext;
extern FILE * yyin;
void yyerror(char * msg);
int yywrap();

int line = 1;

AST * buildIdTree(char * name);
AST * buildIntTree(char * val);
AST * buildFloatTree(char * val);
AST * buildDeclName(char * name);

VALUE_TYPE getTypeNameFromString(char * str);
VALUE_TYPE getTypeNameFromSymbol(AST * ast);

AST * theTree = NULL;
SYMBOL_TABLE * theSymbolTable = NULL;

void insertVar(AST * decl);
void insertFunc(AST * decl);

void pushSymbolTable();

AST * createFunction(AST * header, AST * body);

%}

%code requires {

#include "types.h"

}

%union {
  void * t;
  int i;
  VALUE_TYPE vt;
}

%token NAME
%token INT
%token FLOAT
%token TYPE
%token F_RETURN
%token F_IF
%token F_ELSE
%token F_WHILE
%token F_FOR
%token CMP_EQ
%token CMP_LEQ
%token CMP_NEQ
%token CMP_GEQ
%token PTR
%token L_SIZEOF

%type <t> computation
%type <t> operation
%type <t> mathExpression
%type <t> floatValue
%type <t> intValue
%type <t> name
%type <t> file
%type <t> toplevel
%type <t> varDeclaration
%type <t> varList
%type <t> argList
%type <t> funcCall
%type <t> funcDeclaration
%type <vt> typeName
%type <t> ifStatement
%type <t> condition
%type <i> comperator
%type <t> whileLoop
%type <t> forLoop
%type <t> funcHeader
%type <t> declName
%type <t> deref

%left '+' '-'
%left '*' '/'

%pure-parser

%%

toplevel: file 			{theTree = $1;}
	;
file:
	varDeclaration ';' 						{$$ = astCreateChildren(0, 1, $1);}
	| funcDeclaration						{$$ = astCreateChildren(0, 1, $1);}
	| file varDeclaration ';'				{$$ = astCreateChildren(0, 2, $1, $2);}
	| file funcDeclaration					{$$ = astCreateChildren(0, 2, $1, $2);}
	;

computation:
	operation ';'									{$$ = astCreateChildren(0, 1, $1);}
	| varDeclaration ';'							{$$ = astCreateChildren(0, 1, $1); insertVar($1);}
	| computation operation ';'						{$$ = astCreateChildren(0, 2, $1, $2);}
	| computation varDeclaration ';'				{$$ = astCreateChildren(0, 2, $1, $2); insertVar($2);}
	| F_RETURN mathExpression ';'					{$$ = astCreateChildren(AST_RETURN, 1, $2);}
	| computation F_RETURN mathExpression ';'		{$$ = astCreateChildren(0, 2, $1, astCreateChildren(AST_RETURN, 1, $3));}
	| ifStatement									{$$ = astCreateChildren(0, 1, $1);}
	| computation ifStatement						{$$ = astCreateChildren(0, 2, $1, $2);}
	| whileLoop										{$$ = astCreateChildren(0, 1, $1);}
	| computation whileLoop							{$$ = astCreateChildren(0, 2, $1, $2);}
	| forLoop										{$$ = astCreateChildren(0, 1, $1);}
	| computation forLoop							{$$ = astCreateChildren(0, 2, $1, $2);}
	;

varDeclaration:
	typeName declName						{$$ = astCreateChildren(AST_VAR_DECLARATION, 1, $2); astAddAttribute($$, A_VALUE_TYPE, (VALUE)$1);}
	;

varList:
	varDeclaration						{$$ = astCreateChildren(0, 1, $1); pushSymbolTable(); insertVar($1);}
	| varList ',' varDeclaration		{$$ = astCreateChildren(AST_INSTRUCTION_JOIN, 2, $1, $3); insertVar($3);}
	;


funcDeclaration:
	funcHeader '{' computation '}'				{$$ = createFunction($1, $3);}
	|funcHeader ';'								{$$ = astCreate(0, 0);}
	;

funcHeader:
	typeName declName '(' varList ')'				{$$ = astCreateChildren(1, 2, $2, $4); astAddAttribute($$, A_VALUE_TYPE, (VALUE)$1);insertFunc($$);}
	| typeName declName '(' ')'						{$$ = astCreateChildren(1, 2, $2, astCreate(0, 0)); astAddAttribute($$, A_VALUE_TYPE, (VALUE)$1); insertFunc($$);}
	;

ifStatement:
	F_IF '(' condition ')' '{' computation '}'						{$$ = astCreateChildren(AST_IF, 2, $3, $6);}
	|F_IF '(' condition ')' operation ';'							{$$ = astCreateChildren(AST_IF, 2, $3, $5);}
	;

whileLoop:
	F_WHILE '(' condition ')' '{' computation '}'						{$$ = astCreateChildren(AST_WHILE, 2, $3, $6);}
	|F_WHILE '(' condition ')' operation ';'							{$$ = astCreateChildren(AST_WHILE, 2, $3, $5);}
	;

forLoop:
	F_FOR '(' operation ';' condition ';' operation ')' '{' computation '}'						{$$ = astCreateChildren(AST_FOR, 4, $3, $5, $7, $10);}
	|F_FOR '(' operation ';' condition ';' operation ')' operation ';'							{$$ = astCreateChildren(AST_FOR, 4, $3, $5, $7, $9);}
	;

condition:
	mathExpression comperator mathExpression			{$$ = astCreateChildren($2, 2, $1, $3);}
	;


comperator:
	'<'				{$$ = '<';}
	| '>'			{$$ = '>';}
	| CMP_EQ		{$$ = AST_COMP_EQ;}
	| CMP_LEQ		{$$ = AST_COMP_LEQ;}
	| CMP_NEQ		{$$ = AST_COMP_NEQ;}
	| CMP_GEQ		{$$ = AST_COMP_GEQ;}
	;

operation:
	name '=' mathExpression				{$$ = astCreateChildren('=', 2, $1, $3);}
	|deref '=' mathExpression			{$$ = astCreateChildren('=', 2, $1, $3);}
	|funcCall							{$$ = $1;}
	| name '+' '=' mathExpression		{$$ = astCreateChildren('=', 2, $1, astCreateChildren('+', 2, $1, $4));}
	| name '-' '=' mathExpression		{$$ = astCreateChildren('=', 2, $1, astCreateChildren('-', 2, $1, $4));}
	| name '*' '=' mathExpression		{$$ = astCreateChildren('=', 2, $1, astCreateChildren('*', 2, $1, $4));}
	| name '/' '=' mathExpression		{$$ = astCreateChildren('=', 2, $1, astCreateChildren('/', 2, $1, $4));}
	|name '+' '+'						{$$ = astCreateChildren(AST_INC_POST, 1, $1);}
	|'+' '+' name 						{$$ = astCreateChildren(AST_INC_PRE, 1, $3);}
	;

deref:
	name '[' mathExpression ']'			{$$ = astCreateChildren(AST_DEREF, 2, $1, $3);}
	;

funcCall:
	name '(' argList ')'				{$$ = astCreateChildren(AST_CALL, 2, $1, $3);}
	| name '(' ')'						{$$ = astCreateChildren(AST_CALL, 1, $1);}
	;

argList:
	mathExpression								{$$ = astCreateChildren(0, 1, $1);}
	| argList ',' mathExpression				{$$ = astCreateChildren(0, 2, $1, $3);}
	;

mathExpression:
	floatValue							{$$ = $1;}
	|name								{$$ = $1;}
	|name '+' '+'						{$$ = astCreateChildren(AST_INC_POST, 1, $1);}
	|'+' '+' name 						{$$ = astCreateChildren(AST_INC_PRE, 1, $3);}
	|intValue							{$$ = $1;}
	|funcCall							{$$ = $1;}
	|deref								{$$ = $1;}
	|PTR name							{$$ = astCreateChildren('&', 1, $2);}
	|mathExpression '+' mathExpression	{$$ = astCreateChildren('+', 2, $1, $3);}
	|mathExpression '*' mathExpression	{$$ = astCreateChildren('*', 2, $1, $3);}
	|mathExpression '/' mathExpression	{$$ = astCreateChildren('/', 2, $1, $3);}
	|mathExpression '-' mathExpression	{$$ = astCreateChildren('-', 2, $1, $3);}
	| '(' mathExpression ')'			{$$ = $2;}
	;

floatValue:
	FLOAT								{$$ = buildFloatTree(yytext);}
	;

intValue:
	INT									{$$ = buildIntTree(yytext);}
	;

name:
	NAME								{$$ = buildIdTree(yytext);}
	;

declName:
	NAME								{$$ = buildDeclName(yytext);}
	;

typeName:
	TYPE								{$$ = getTypeNameFromString(yytext);}
	|PTR typeName						{$$ = $2; $$.ptrLvl++;}
	|name								{$$ = getTypeNameFromSymbol($1);}
	;

%%

int yywrap() {
	return 1;
}

void yyerror(char * msg) {

	fprintf(stderr, "Error:\n%s near '%s' on line %d\n", msg, yytext, line);

}

VALUE_TYPE getTypeNameFromString(char * str) {

	VALUE_TYPE t;
	t.baseId = getTypeId(str);
	t.ptrLvl = 0;

	return t;

}

VALUE_TYPE getTypeNameFromSymbol(AST * ast) {

	VALUE_TYPE t;
	t.baseId = getTypeId(astGetAttributeValue(ast, A_VALUE).str);
	t.ptrLvl = 0;

	return t;

}

AST * buildDeclName(char * name) {
	VALUE v;
	v.str = malloc(sizeof(char) * (strlen(name)+1));
	strcpy(v.str, name);

	AST * ast = astCreate(AST_ID,0);

	astAddAttribute(ast, A_VALUE, v);

	return ast;
}

AST * buildIdTree(char * name) {

	VALUE v;
	v.str = malloc(sizeof(char) * (strlen(name)+1));
	strcpy(v.str, name);

	SYMBOL_INFO info = symbolTableLookup(theSymbolTable, v.str);

	if (!info.vType.baseId && !getTypeId(name)) {
		make_error(fprintf(stderr, "Unknown symbol %s\n", name));

	}

	AST * ast = astCreate(AST_ID,0);

	astAddAttribute(ast, A_VALUE, v);
	astAddAttribute(ast, A_VALUE_TYPE, (VALUE) info.vType);

	return ast;
}

AST * buildIntTree(char * name) {
	VALUE v;
	v.str = malloc(sizeof(char) * (strlen(name)+1));
	strcpy(v.str, name);
	v.str[strlen(name)] = 0;

	VALUE_TYPE vt;
	vt.baseId = getTypeId("int");
	vt.ptrLvl = 0;

	AST * ast = astCreate(AST_CONST,0);
	astAddAttribute(ast, A_VALUE, v);
	astAddAttribute(ast, A_VALUE_TYPE, (VALUE) vt);
}

AST * buildFloatTree(char * name) {

	VALUE v;
	v.str = malloc(sizeof(char) * (strlen(name)+1));
	strcpy(v.str, name);
	v.str[strlen(name)] = 0;

	VALUE_TYPE vt;
	vt.baseId = getTypeId("double");
	vt.ptrLvl = 0;

	AST * ast = astCreate(AST_CONST,0);
	astAddAttribute(ast, A_VALUE, v);
	astAddAttribute(ast, A_VALUE_TYPE, (VALUE) vt);

	return ast;
}

void insertVar(AST * decl) {

	int count;
	AST ** children = astGetChildren(decl, &count);


	char * name = astGetAttributeValue(children[0], A_VALUE).str;
	SYMBOL_INFO info;
	info.oType = O_VARIABLE;
	info.vType = astGetAttributeValue(decl, A_VALUE_TYPE).vt;
	info.memOffset = 0;
	info.name = name;
	symbolTableInsert(theSymbolTable, name, info);

}

void pushSymbolTable() {
	theSymbolTable = symbolTableCreate(theSymbolTable);
}

void insertFunc(AST * decl) {

	int count;
	AST ** children = astGetChildren(decl, &count);

	char * name = astGetAttributeValue(children[0], A_VALUE).str;
	SYMBOL_INFO info;
	info.oType = O_FUNCTION;
	info.vType = astGetAttributeValue(decl, A_VALUE_TYPE).vt;
	info.memOffset = 0;
	info.name = name;
	symbolTableInsert(symbolTableGetParrent(theSymbolTable), name, info);

}

AST * createFunction(AST * header, AST * body) {

	int count;
	AST ** children = astGetChildren(header, &count);

	AST * func = astCreateChildren(AST_FUNC_DECL, 3, children[0], children[1], body);
	astAddAttribute(func, A_VALUE_TYPE, (VALUE)symbolTableLookup(theSymbolTable, astGetAttributeValue(children[0], A_VALUE).str).vType);
	astAddAttribute(func, A_SYM_TABLE, (VALUE) (void*)theSymbolTable);

	theSymbolTable = symbolTableGetParrent(theSymbolTable);

	return func;

}

AST * getSyntaxTreeFromFile(FILE * file) {

	theSymbolTable = symbolTableCreate(NULL);

	yyin = file;
	yyparse();

	return theTree;

}



