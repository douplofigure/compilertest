#include <stdio.h>
#include <stdlib.h>

typedef union convert_u {
    long l;
    double d;
} CONVERT;

void printInt(int val) {
    printf("%d\n", val);
}

void printFloat(float val) {
    printf("%f\n", val);
}

void printDouble(double val) {
    printf("%g\n", val);
}

void printLong(long val) {

    printf("long = %ld , %lx\n", val, val);
}

void printChar(char chr) {

    printf("'%c'\n", chr);

}

void printString(char * str) {
    printf("%s", str);
}

void * getMemInternal(size_t size) {
    void * ptr = malloc(size);
    return ptr;
}

void * getMem(int size) {
    return getMemInternal(size);
}
