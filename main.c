#include <stdio.h>

#include "y.tab.h"
#include "ast.h"
#include "util.h"
#include "automaton.h"

#include "prettyprint.h"

extern AST * theTree;
extern SYMBOL_TABLE * theSymbolTable;

extern unsigned int getHash(char * str);
extern AST * getSyntaxTreeFromFile(FILE * file);

int main(int argc, char ** argv) {

    if (argc < 3) make_error(fprintf(stderr, "Missing argumets -- usage:\n\t%s <regex> <string_to_test>\n", argv[0]))

    /*
    NFA * nfa = buildNFA(argv[1]);

    nfaSaveGraph(nfa, "nfa.dot");
    nfaSimplyfy(nfa);
    nfaPrint(nfa);
    nfaSaveGraph(nfa, "new.dot");

    DFA_STATE * dfa = buildDFA(nfa);

    printf("String '%s' does %smatch regular expression '%s'\n", argv[2], dfaMatch(dfa, argv[2]) ? "" : "not ", argv[1]);

    //return 0;
    */

    if (argc < 2) make_error(fprintf(stderr, "No input given\n"));

    insertDefaultTypes();
    FILE * inFile = fopen(argv[1], "r");

    AST * ast = getSyntaxTreeFromFile(inFile);
    fclose(inFile);

    astPrint(ast, 0);

    CODE_STREAM * stream = utilCreateCodeStream(ast, theSymbolTable);

    FILE * file = stdout;
    if (argc >= 3)
        file = fopen(argv[2], "w");
    writeAssemblyToFile(file, stream, theSymbolTable);
    if (argc >= 3)
        fclose(file);

}
