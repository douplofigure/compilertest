#include "automaton.h"

#include <stdlib.h>
#include <stdio.h>

#include "prettyprint.h"

typedef struct nfa_stack_frame_t {

    NFA * nfa;
    struct nfa_stack_frame_t * prev;

} NFA_STACK_FRAME;

typedef struct nfa_Stack_t {

    NFA_STACK_FRAME * top;

} NFA_STACK;

typedef struct input_stack_frame_t {
    int value;
    struct input_stack_frame_t * prev;
} INPUT_STACK_FRAME;

typedef struct input_stack_t {
    INPUT_STACK_FRAME * top;
} INPUT_STACK;

INPUT_STACK * inputStackCreate() {

    INPUT_STACK * stack = malloc(sizeof(INPUT_STACK));
    stack->top = NULL;
    return stack;

}

void inputStackPrint(INPUT_STACK * stack) {

    printf("Stack: ");
    INPUT_STACK_FRAME * frame = stack->top;

    while (frame) {

        if (frame->value > 0)
            printf("'%c' ", frame->value);
        else
            printf("%d ", frame->value);
        frame = frame->prev;

    }

    printf("\n");

}

void inputStackPush(INPUT_STACK * stack, int value) {

    INPUT_STACK_FRAME * frame = malloc(sizeof(INPUT_STACK_FRAME));
    frame->prev = stack->top;
    frame->value = value;

    stack->top = frame;

}

int inputStackPop(INPUT_STACK * stack) {
    if (!stack->top) return 0;
    INPUT_STACK_FRAME * frame = stack->top;
    int val = frame->value;
    stack->top = frame->prev;
    free(frame);
    return val;
}

int inputStackLook(INPUT_STACK * stack) {
    if (!stack->top) return 0;
    return stack->top->value;
}

NFA_STACK * nfaStackCreate() {

    NFA_STACK * stack = malloc(sizeof(NFA_STACK));

    stack->top = NULL;

    return stack;

}

void nfaStackPush(NFA_STACK * stack, NFA * nfa) {

    NFA_STACK_FRAME * frame = malloc(sizeof(NFA_STACK_FRAME));
    frame->prev = stack->top;
    frame->nfa = nfa;

    stack->top = frame;

}

NFA * nfaStackPop(NFA_STACK * stack) {

    if (!stack->top) return NULL;

    NFA * nfa = stack->top->nfa;

    NFA_STACK_FRAME * frame = stack->top;

    stack->top = frame->prev;

    free(frame);

    return nfa;

}

void automatonStackSimplify(INPUT_STACK * iStack, NFA_STACK * stack) {

    inputStackPrint(iStack);

    int lastInput;
    int stackTop;
    NFA * nfa;

    if (inputStackLook(iStack) > 0) {
        lastInput = inputStackPop(iStack);
        stackTop = inputStackPop(iStack);
        if (stackTop == -1) {

            printf("Appending single char\n");
            nfa = nfaMergeAppend(nfaStackPop(stack), nfaCreateSingleChar(lastInput));
            printf("Append ok\n");
            nfaStackPush(stack, nfa);
            inputStackPush(iStack, -1);

        } else if (stackTop < -1) {

            nfa = nfaCreateSingleChar(lastInput);

            nfaStackPush(stack, nfa);

            inputStackPush(iStack, stackTop);
            inputStackPush(iStack, -1);


        } else if (stackTop) {

            nfa = nfaMergeAppend(nfaCreateSingleChar(stackTop), nfaCreateSingleChar(lastInput));
            nfaStackPush(stack, nfa);
            inputStackPush(iStack, -1);

        } else {

            inputStackPush(iStack, 0);
            inputStackPush(iStack, lastInput);

        }
    }
    if (inputStackLook(iStack) < 0) {

        printf("Negative lookup\n");

        lastInput = inputStackPop(iStack);
        int op = inputStackPop(iStack);
        stackTop = inputStackPop(iStack);

        if (op == -2 && stackTop == -1) {

            nfa = nfaMergeOr(nfaStackPop(stack), nfaStackPop(stack));
            nfaStackPush(stack, nfa);
            inputStackPush(iStack, stackTop);

        } else if (op == -2 && stackTop) {

            nfa = nfaMergeOr(nfaStackPop(stack), nfaCreateSingleChar(stackTop));
            nfaStackPush(stack, nfa);
            inputStackPush(iStack, -1);

        } else if (op > 0 && lastInput == -1) {


            nfa = nfaMergeAppend(nfaCreateSingleChar(op), nfaStackPop(stack));
            nfaStackPush(stack, nfa);
            inputStackPush(iStack, -1);

        } else {
            if (stackTop)
            inputStackPush(iStack, stackTop);
            if (op)
            inputStackPush(iStack, op);
            inputStackPush(iStack, lastInput);

        }

    }
    printf("Exit: ");
    inputStackPrint(iStack);

}

int nfaBuild(NFA_STACK * stack, char * data) {

    char c;
    char * str = data;
    int parLvl = 0;

    //printf("data='%s'\n", data);

    NFA * nfa;
    NFA * stackNfa;

    INPUT_STACK * iStack = inputStackCreate();
    int lastInput;
    int stackTop;

    while (c = *(str++)) {

        //printf("*str = %c\n", c);

        switch (c) {

            case '(':
                automatonStackSimplify(iStack, stack);
                inputStackPush(iStack, 0);
                break;

            case ')':

                automatonStackSimplify(iStack, stack);
                lastInput = inputStackPop(iStack);
                inputStackPop(iStack);
                inputStackPush(iStack, lastInput);
                break;

            case '*':

                printf("OPERATOR *\n");
                lastInput = inputStackPop(iStack);
                if (!lastInput) make_error(fprintf(stderr, "unexpected * in regex\n"));

                if (lastInput == -1) {

                    nfa = nfaOperatorStar(nfaStackPop(stack));

                } else {

                    nfa = nfaOperatorStar(nfaCreateSingleChar(lastInput));

                }

                int stackTop = inputStackPop(iStack);
                if (stackTop == -1) {
                    nfa = nfaMergeAppend(nfaStackPop(stack), nfa);
                } else if (stackTop) {
                    inputStackPush(iStack, stackTop);
                }

                nfaStackPush(stack, nfa);
                inputStackPush(iStack, -1);

                break;

            case '[':
                nfa = nfaCreateCharRange(*(str), *(str+1));
                str += 3;
                nfaStackPush(stack, nfa);
                inputStackPush(iStack, -1);
                break;

            case '\\':
                str++;

            case '+':
            case '|':
                automatonStackSimplify(iStack, stack);
                inputStackPush(iStack, -2);
                break;


            default:
                automatonStackSimplify(iStack, stack);
                inputStackPush(iStack, c);
                break;

        }

        //inputStackPrint(iStack);

    }

    automatonStackSimplify(iStack, stack);


    return str - data-1;

}

/*int nfaBuild(NFA_STACK * stack, char * data) {

    char c;
    char * str = data;
    int parLvl = 0;

    printf("data='%s'\n", data);

    //NFA * nfa = nfaCreateSingleChar(*(str++));

    NFA * nfa;

    while (c = *(str++)) {

        printf("*str = %d\n", *str);

        switch (c) {

            case '(':
                nfaStackPush(stack, NULL);
                str += nfaBuild(stack, str);
                break;

            case ')':
                printf("Returnning\n");
                return str-data;

            case '+':
            case '|':
                nfa = nfaStackPop(stack);
                str += nfaBuild(stack, str);
                nfa = nfaMergeOr(nfa, nfaStackPop(stack));
                nfaStackPush(stack, nfa);
                printf("OPERATOR |\n");
                break;


            case '*':
                nfa = nfaOperatorStar(nfaStackPop(stack));
                nfaStackPush(stack, nfa);
                printf("OPERATOR *\n");
                break;

            case '[':
                printf("Creating nfa with range (%c, %c)\n", *(str), *(str+1));
                nfa = nfaCreateCharRange(*(str), *(str+1));
                str += 3;
                nfaStackPush(stack, nfa);
                break;

            case '\\':
                str++;
            default:
                if (*(str) != '*') {
                    nfa = nfaStackPop(stack);
                    if (nfa) {
                        nfa = nfaMergeAppend(nfa, nfaCreateSingleChar(c));
                    } else {
                        nfa = nfaCreateSingleChar(c);
                    }
                } else {
                    nfa = nfaCreateSingleChar(c);
                }
                nfaStackPush(stack, nfa);
                break;

        }

    }

    return str - data-1;

}*/

NFA * buildNFA(char * str) {

    NFA_STACK * stack = nfaStackCreate();
    int readData = nfaBuild(stack, str);

    NFA * res = nfaStackPop(stack);
    NFA * nfa;

    while (nfa = nfaStackPop(stack)) {
        nfaPrint(nfa);
        res = nfaMergeAppend(nfa, res);

    }
    free(stack);

    return res;

}
