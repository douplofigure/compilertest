#ifndef PRETTYPRINT_H_INCLUDED
#define PRETTYPRINT_H_INCLUDED

#include <stdlib.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

extern int line;
#define make_error(x) {fprintf(stderr,  "%sError on line %d: (%s : %d)  ", KRED, line, __FILE__, __LINE__); x; fprintf(stderr, KNRM); exit(1);}
#define make_warning(x) {fprintf(stderr, KMAG); x; fprintf(stderr, KNRM);}

#endif // PRETTYPRINT_H_INCLUDED
