#include "registry.h"

#include <stdio.h>
#include "codestream.h"
#include "types.h"

#include "prettyprint.h"

static char * registries[] = {

    /*int*/
    /// 64 bit
    "%rax",
    "%rcx",
    "%rdx",
    "%rsi",
    "%rdi",
    "%r8",
    "%r9",
    "%r10",
    "%r11",

    /// 32 bit
    "%eax",
    "%ecx",
    "%edx",
    "%esi",
    "%edi",
    "%r8d",
    "%r9d",
    "%r10d",
    "%r11d",

    /// 16 bit
    "%ax",
    "%cx",
    "%dx",
    "%si",
    "%di",
    "%r8w",
    "%r9w",
    "%r10w",
    "%r11w",

    /// 8 bit
    "%al",
    "%cl",
    "%dl",
    "%sil",
    "%dil",
    "%r8b",
    "%r9b",
    "%r10b",
    "%r11b",

    /*float*/
    "%xmm0",
    "%xmm1",
    "%xmm2",
    "%xmm3",
    "%xmm4",
    "%xmm5",
    "%xmm6",
    "%xmm7",

};

static int callregs[6] = {4,3,2,1,5,6};

#define INT_REG_COUNT 9
#define FP_REG_COUNT 8

#define FP_REG_OFFSET 36

static CODE_ARG intRegs[INT_REG_COUNT];
static CODE_ARG fpRegs[FP_REG_COUNT];
static int fpRequests[FP_REG_COUNT];
static int intRequests[INT_REG_COUNT];

int integerRegOffset(int valSize) {

    if (!valSize)return 0;

    int offset = 0;
    switch (valSize) {

        case 8:
            break;

        case 4:
            offset = INT_REG_COUNT;
            break;

        case 2:
            offset = INT_REG_COUNT*2;
            break;

        case 1:
            offset = INT_REG_COUNT*3;
            break;

        default:
            offset = INT_REG_COUNT;
            break;

    }

    return offset;

}

char * getRegistryName(int regNum) {

    if (regNum < 0) make_error(fprintf(stderr, "Bad Registry index %d\n", regNum));
    return registries[regNum];

}

int getReg(VALUE_TYPE valType, CODE_ARG arg) {

    static int rqNum;

    rqNum++;
    int valSize = getTypeSize(valType);
    int isFP = isFloatingPoint(valType);

    if (isFP) {
        for (int i = 0; i < FP_REG_COUNT; ++i)
            if (fpRegs[i].type == arg.type && arg.value.ptr == fpRegs[i].value.ptr) {

                fpRequests[i] = rqNum;
                return i + FP_REG_OFFSET;

            }

        for (int i = 0; i < FP_REG_COUNT; ++i)
            if (!fpRegs[i].type) {

                fpRequests[i] = rqNum;
                return i + FP_REG_OFFSET;

            }
        int minVal = rqNum;
        int minReg = 0;
        for (int i = 0; i < FP_REG_COUNT; ++i)
            if (minVal > fpRequests[i]&& (fpRegs[i].type == ARG_SYMBOL || fpRegs[i].type == ARG_CONSTANT)) {
                minVal = fpRequests[i];
                minReg = i;
            }

        ///SAVE Current Value (just to be safe)
        fpRequests[minReg] = rqNum;
        fpRegs[minReg] = arg;

        return minReg + FP_REG_OFFSET;

    }

    int offset = integerRegOffset(valSize);
    for (int i = 0; i < INT_REG_COUNT; ++i)
        if (intRegs[i].type == arg.type && arg.value.ptr == intRegs[i].value.ptr) {

            intRequests[i] = rqNum;
            return i + offset;

        }

    for (int i = 0; i < INT_REG_COUNT; ++i)
        if (!intRegs[i].type) {

            intRequests[i] = rqNum;
            return i + offset;

        }
    int minVal = rqNum;
    int minReg = 0;
    for (int i = 0; i < INT_REG_COUNT; ++i)
        if (minVal > intRequests[i] && (intRegs[i].type == ARG_SYMBOL || intRegs[i].type == ARG_CONSTANT)) {
            minVal = intRequests[i];
            minReg = i;
        }


    intRequests[minReg] = rqNum;
    intRegs[minReg] = arg;

    return minReg+offset;
}

void registryUpdate(int regNum, CODE_ARG arg) {

    if (regNum >= FP_REG_OFFSET) {

        fpRegs[regNum-FP_REG_OFFSET] = arg;
        return;

    }

    intRegs[regNum % INT_REG_COUNT] = arg;

}

void wipeRegistries() {
    for (int i = 0; i < INT_REG_COUNT; ++i) {
        intRegs[i] = (CODE_ARG) {0};
    }

    for (int i = 0; i < FP_REG_COUNT; ++i) {
        fpRegs[i] = (CODE_ARG) {0};
    }
}

int registryGetArgument(VALUE_TYPE t, int argNum) {

    if (isFloatingPoint(t)) {

        return FP_REG_OFFSET + argNum;

    }

    return integerRegOffset(getTypeSize(t)) + callregs[argNum];

}

int registryGetReturn(VALUE_TYPE t) {

    if (isFloatingPoint(t)) {

        return FP_REG_OFFSET;

    }

    return integerRegOffset(getTypeSize(t));

}

int registryContains(int i, CODE_ARG arg) {

    if (isFloatingPoint(arg.valType)) {

        return intRegs[i-FP_REG_OFFSET].type == arg.type && intRegs[i-FP_REG_OFFSET].value.ptr == arg.value.ptr;

    }

    return intRegs[i].type == arg.type && intRegs[i].value.ptr == arg.value.ptr;

}
