#ifndef REGISTRY_H_INCLUDED
#define REGISTRY_H_INCLUDED

#include "types.h"
#include "codestream.h"

char * getRegistryName(int regNum);
void registryUpdate(int regNum, CODE_ARG arg);
int getReg(VALUE_TYPE t, CODE_ARG arg);
void wipeRegistries();

int registryGetArgument(VALUE_TYPE t, int argNum);
int registryGetReturn(VALUE_TYPE t);
int registryContains(int i, CODE_ARG arg);

#endif // REGISTRY_H_INCLUDED
