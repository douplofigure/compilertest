#include "set.h"

#include <stdlib.h>

typedef struct int_set_entry_t {

    int val;
    struct int_set_entry_t * next;

} INT_SET_ENTRY;

struct int_set_t {

    INT_SET_ENTRY * first;

};

typedef struct int_set_set_entry_t {

    INT_SET * val;
    void * data;
    struct int_set_set_entry_t * next;

} INT_SET_SET_ENTRY;

struct int_set_set_t {

    INT_SET_SET_ENTRY * first;

};

INT_SET * intSetCreate() {

    INT_SET * set = malloc(sizeof(INT_SET));

    set->first = NULL;

    return set;

}

void intSetDestroy(INT_SET * set) {

    INT_SET_ENTRY * entry = set->first;

    while (entry) {
        INT_SET_ENTRY * tmp = entry;
        entry = entry->next;
        free(tmp);
    }
    free(set);

}

int intSetContains(INT_SET * set, int val) {

    if (!set->first) return 0;

    INT_SET_ENTRY * entry = set->first;

    while (entry) {
        if (entry->val == val) return 1;

        entry = entry->next;
    }

    return 0;

}

void intSetInsert(INT_SET * set, int val) {

    if (intSetContains(set, val)) return;

    INT_SET_ENTRY * entry = malloc(sizeof(INT_SET_ENTRY));
    entry->next = set->first;
    entry->val = val;

    set->first = entry;

}

void intSetRemove(INT_SET * set, int val) {

    INT_SET_ENTRY * entry = set->first;

    while (entry) {
        if (entry->next && entry->next->val == val) {

            INT_SET_ENTRY * tmp = entry->next;
            entry->next = tmp->next;
            free(tmp);

        }
        else
            entry = entry->next;
    }
}

int intSetEqual(INT_SET * set1, INT_SET * set2) {

    INT_SET_ENTRY * entry = set1->first;

    while (entry) {
        if (!intSetContains(set2, entry->val))
            return 0;
        entry = entry->next;
    }

    entry = set2->first;

    while (entry) {
        if (!intSetContains(set1, entry->val))
            return 0;
        entry = entry->next;
    }

    return 1;

}

INT_SET * intSetMerge(INT_SET * set1, INT_SET * set2, int destroy) {

    INT_SET_ENTRY * entry = set2->first;
    while (entry) {

        if (!intSetContains(set1, entry->val))
            intSetInsert(set1, entry->val);

        entry = entry->next;

    }

    if (destroy) intSetDestroy(set2);

    return set1;

}

INT_SET_SET * intSetSetCreate() {

    INT_SET_SET * set = malloc(sizeof(INT_SET_SET));

    set->first = NULL;

    return set;

}

void intSetSetDestroy(INT_SET_SET * set, int purge) {

    INT_SET_SET_ENTRY * entry = set->first;

    while (entry) {
        INT_SET_SET_ENTRY * tmp = entry;
        entry = entry->next;
        if (purge) intSetDestroy(entry->val);
        free(tmp);
    }

    free(set);

}

int intSetSetContains(INT_SET_SET * set, INT_SET * val) {

    INT_SET_SET_ENTRY * entry = set->first;

    while (entry) {
        if (intSetEqual(entry->val, val)) return 1;

        entry = entry->next;
    }

    return 0;

}

void intSetSetInsert(INT_SET_SET * set, INT_SET * val, void * data) {

    if (intSetSetContains(set, val)) return;

    INT_SET_SET_ENTRY * entry = malloc(sizeof(INT_SET_SET_ENTRY));
    entry->next = set->first;
    entry->val = val;
    entry->data = data;

    set->first = entry;

}

void intSetSetRemove(INT_SET_SET * set, INT_SET * val) {

    INT_SET_SET_ENTRY * entry = set->first;

    while (entry) {
        if (entry->next && intSetEqual(entry->next->val, val)) {

            INT_SET_SET_ENTRY * tmp = entry->next;
            entry->next = tmp->next;
            free(tmp);

        }
        else
            entry = entry->next;
    }
}

int intSetSetEqual(INT_SET_SET * set1, INT_SET_SET * set2) {

    INT_SET_SET_ENTRY * entry = set1->first;

    while (entry) {
        if (!intSetSetContains(set2, entry->val))
            return 0;
        entry = entry->next;
    }

    entry = set2->first;

    while (entry) {
        if (!intSetSetContains(set1, entry->val))
            return 0;
        entry = entry->next;
    }

    return 1;

}

void * intSetSetGetData(INT_SET_SET * set, INT_SET * val) {

    INT_SET_SET_ENTRY * entry = set->first;

    while (entry) {

        if (intSetEqual(entry->val, val))
            return entry->data;

        entry = entry->next;

    }

    return NULL;

}

INT_SET ** intSetSetAsArray(INT_SET_SET * set, int * setCount) {

    int count = 0;

    INT_SET_SET_ENTRY * entry = set->first;

    while (entry) {
        count++;
        entry = entry->next;
    }

    *setCount = count;

    int index = 0;
    INT_SET ** sets = malloc(sizeof(INT_SET *) * count);

    entry = set->first;

    while (entry) {
        sets[index++] = entry->val;
        entry = entry->next;
    }

    return sets;

}

void intSetPrintFile(INT_SET * set, FILE * file) {

    fprintf(file, "{");
    INT_SET_ENTRY * entry = set->first;
    while (entry) {

        fprintf(file, "%d", entry->val);
        entry = entry->next;

        if (entry) {
            fprintf(file, ", ");
        }

    }
    fprintf(file, "}");

}
