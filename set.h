#ifndef SET_H_INCLUDED
#define SET_H_INCLUDED

#include <stdio.h>

///{1, 3}
typedef struct int_set_t INT_SET;

///{{0, 1, 2}, {1, 2}}
typedef struct int_set_set_t INT_SET_SET;

INT_SET * intSetCreate();
void intSetDestroy(INT_SET * set);
int intSetContains(INT_SET * set, int val);
void intSetInsert(INT_SET * set, int val);
void intSetRemove(INT_SET * set, int val);
int intSetEqual(INT_SET * set1, INT_SET * set2);
INT_SET * intSetMerge(INT_SET * set1, INT_SET * set2, int destroy);

void intSetPrintFile(INT_SET * set, FILE * file);

INT_SET_SET * intSetSetCreate();
void intSetSetDestroy(INT_SET_SET * set, int purge);
int intSetSetContains(INT_SET_SET * set, INT_SET * val);
void intSetSetInsert(INT_SET_SET * set, INT_SET * val, void * data);
void * intSetSetGetData(INT_SET_SET * set, INT_SET * val);
void intSetSetRemove(INT_SET_SET * set, INT_SET * val);
int intSetSetEqual(INT_SET_SET * set1, INT_SET_SET * set2);

INT_SET ** intSetSetAsArray(INT_SET_SET * set, int * setCount);

#endif // SET_H_INCLUDED
