#include "smartarray.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "prettyprint.h"

struct smart_array_t {

    int elementSize;
    int elementCount;
    int bufferSize;
    void * data;

};

SMART_ARRAY * saCreate(int elemSize, int elemCount) {

    printf("Creating array with typeSize: %d and count: %d\n", elemSize, elemCount);

    SMART_ARRAY * array = malloc(sizeof(SMART_ARRAY));
    array->elementCount = elemCount;
    array->elementSize = elemSize;
    array->bufferSize = elemCount;

    array->data = elemCount ? malloc(elemCount * elemSize) : NULL;

    return array;

}

void * saGetData(SMART_ARRAY * array, int * elemCount) {

    *elemCount = array->elementCount;
    return array->data;

}

void saPutData(SMART_ARRAY * array, int index, void * data) {

    if (index >= array->bufferSize) make_error(fprintf(stderr, "Trying to put data outside of array\n"));
    memcpy(array->data + (index * array->elementSize), data, array->elementSize);

}

void saAppend(SMART_ARRAY * array, void * data) {
    printf("Before append: size=%d, count=%d\n", array->elementSize, array->elementCount);
    if (!array->data) {
        array->data = malloc(array->elementSize);
        array->elementCount = 0;
        array->bufferSize = 1;
    } else if (array->elementCount >= array->bufferSize) {
        array->data = realloc(array->data, 2 * array->elementSize * array->bufferSize);
        array->bufferSize *= 2;
    }

    memcpy(array->data + (array->elementCount++ * array->elementSize), data, array->elementSize);
    printf("After append: size=%d, count=%d\n", array->elementSize, array->elementCount);
}

void saConcat(SMART_ARRAY * a1, SMART_ARRAY * a2) {

    printf("Concat Arrays (%d + %d) * %d | %d\n", a1->elementCount, a2->elementCount, a1->elementSize, a2->elementSize);

    if (a1->elementSize != a2->elementSize)
        make_error(fprintf(stderr, "can not concatenate Array of different type sizes: %d and %d\n", a1->elementSize, a2->elementSize));
    if (a1->elementSize * (a1->elementCount + a2->elementCount) >= a1->bufferSize * a1->elementSize)
        a1->data = realloc(a1->data, a1->elementSize * (a1->elementCount + a2->elementCount));
    memcpy(a1->data + (a1->elementSize * a1->elementCount), a2->data, a2->elementSize * a2->elementCount);

    a1->elementCount += a2->elementCount;

}

void saRemove(SMART_ARRAY * array, int index) {

    if (index < 0 || index >= array->elementCount) return;

    memcpy(array->data + (array->elementSize * index), array->data + (array->elementSize * --array->elementCount), array->elementSize);

}
