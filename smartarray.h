#ifndef SMARTARRAY_H_INCLUDED
#define SMARTARRAY_H_INCLUDED

typedef struct smart_array_t SMART_ARRAY;

SMART_ARRAY * saCreate(int elemSize, int elemCount);

void * saGetData(SMART_ARRAY * array, int * elemCount);

void saPutData(SMART_ARRAY * array, int index, void * data);

void saAppend(SMART_ARRAY * array, void * data);

void saConcat(SMART_ARRAY * a1, SMART_ARRAY * a2);
void saRemove(SMART_ARRAY * array, int index);

#endif // SMARTARRAY_H_INCLUDED
