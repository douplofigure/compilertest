#include "types.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "prettyprint.h"

struct type_dict_t {

    int indexedTypes;
    int mapSize;
    VAL_TYPE * types;

};

extern unsigned int getHash(char * str);
extern int getNextPrime(int n);

static TYPE_DICT * theTypeDict;

TYPE_DICT * typedictCreate(int mapSize) {

    TYPE_DICT * dict = malloc(sizeof(TYPE_DICT));
    dict->indexedTypes = 0;
    dict->mapSize = getNextPrime(mapSize);

    dict->types = malloc(sizeof(VAL_TYPE) * dict->mapSize);

    for (int i = 0; i < dict->mapSize; ++i) {

        dict->types[i].name = NULL;
        dict->types[i].size = 0;

    }

    return dict;

}

unsigned int typeInsert(char * name, int size, unsigned int attributes, readFunc * func, int precedence) {

    unsigned int hash = getHash(name);
    if (!theTypeDict)
        theTypeDict = typedictCreate(256);


    while (theTypeDict->types[hash % theTypeDict->mapSize].name) {
        if (!strcmp(theTypeDict->types[hash % theTypeDict->mapSize].name, name)) {
            return (hash % theTypeDict->mapSize) + 1;
        }
        hash++;

    }

    unsigned int pos = hash % theTypeDict->mapSize;

    theTypeDict->types[pos].name = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(theTypeDict->types[pos].name, name);
    theTypeDict->types[pos].name[strlen(name)] = 0;
    theTypeDict->types[pos].size = size;
    theTypeDict->types[pos].attributes = attributes;
    theTypeDict->types[pos].func = func;
    theTypeDict->types[pos].precedence = precedence;

    return pos+1;

}

unsigned int getTypeId(char * name) {

    unsigned int hash = getHash(name);
    if (!theTypeDict)
        make_error(fprintf(stderr, "No type dictionary found\n"));

    while (theTypeDict->types[hash % theTypeDict->mapSize].name && strcmp(theTypeDict->types[hash % theTypeDict->mapSize].name, name))
        hash++;

    if (theTypeDict->types[hash % theTypeDict->mapSize].name)
        return (hash % theTypeDict->mapSize) + 1;

    make_error(fprintf(stderr, "Unknown type %s\n", name));
    return 0;

}

VAL_TYPE getTypeInfo(unsigned int typeId) {

    if(typeId >= theTypeDict->mapSize) make_error(fprintf(stderr, "Unknown type id %d\n", typeId));

    return theTypeDict->types[typeId-1];

}

VALUE parseDouble(char * str) {

    VALUE v;
    v.d = atof(str);
    return v;

}

void insertDefaultTypes() {

    typeInsert("int",   sizeof(int),   0, (readFunc*) &atoi, 2);
    typeInsert("long",  sizeof(long),  0, (readFunc*) &atol, 3);
    typeInsert("char",  sizeof(char),  0, (readFunc*) &atoi, 0);
    typeInsert("short", sizeof(short), 0, (readFunc*) &atoi, 1);

    typeInsert("double", sizeof(double), TA_FP_TYPE, parseDouble, 4);

    typeInsert("void", 0, 0, (readFunc*) NULL, 0);

}

int getTypeSize(VALUE_TYPE t) {

    if (t.ptrLvl) return sizeof(void *);

    return getTypeInfo(t.baseId).size;

}

int isFloatingPoint(VALUE_TYPE t) {

    if (t.ptrLvl) return 0;
    return getTypeInfo(t.baseId).attributes & TA_FP_TYPE;

}

VALUE typeConvert(VALUE_TYPE dest, VALUE_TYPE origin, VALUE val) {

    if (dest.ptrLvl == origin.ptrLvl && dest.baseId == origin.baseId)
        return val;

    if (dest.ptrLvl && origin.ptrLvl) {
        make_warning(fprintf(stderr, "Pointer conversion (hope you know what you are doing :-)!)\n"));
        return val;
    }

    VAL_TYPE destInfo = getTypeInfo(dest.baseId);
    VAL_TYPE origInfo = getTypeInfo(origin.baseId);

    if (destInfo.attributes & TA_COMPLEX)
        make_error(fprintf(stderr, "Can not convert to complex type\n"));

    if (destInfo.attributes & TA_FP_TYPE && !(origInfo.attributes & TA_FP_TYPE))
        return val;

    return val;

}

PRINT_VALUE getPrintValue(VALUE_TYPE valType, PRINT_TYPE * pType, VALUE val) {

    PRINT_VALUE res;

    if (valType.ptrLvl) {
        *pType = PT_DOUBLE_LONG;
        res.str = (void *) atol(val.str);
    }
    if (valType.baseId == getTypeId("char") && valType.ptrLvl == 1) {
        *pType = PT_STRING;
        res.str = val.str;
    }

    VAL_TYPE typeInfo = getTypeInfo(valType.baseId);

    if (typeInfo.size > 4) {
        *pType = PT_DOUBLE_LONG;
    } else {
        *pType = PT_LONG;
    }
    PRINT_VALUE v;
    if (typeInfo.func)
        v.l = typeInfo.func(val.str).l;
    //printf("double=%g\n", *((double *) & val.l));
    return v;

}

VALUE_TYPE typeGetSmallestCommon(VALUE_TYPE t1, VALUE_TYPE t2) {

    if (!t1.baseId) return t2;
    if (!t2.baseId) return t1;
    if (t1.ptrLvl && t2.ptrLvl) return t1;

    VAL_TYPE t1Info = getTypeInfo(t1.baseId);
    VAL_TYPE t2Info = getTypeInfo(t2.baseId);

    return (t1Info.precedence > t2Info.precedence) ? t1 : t2;

}
