#ifndef _TYPES_H
#define _TYPES_H

typedef struct dual_int_t {
    int h;
    int l;
} DUAL_INT;

typedef enum value_type_e {

    PT_LONG = 1,
    PT_DOUBLE_LONG,

    PT_STRING,

} PRINT_TYPE;

typedef union print_value_t {

    unsigned long int l;
    char * str;
    DUAL_INT v;

} PRINT_VALUE;

typedef enum type_attribute_e {

    TA_FP_TYPE = 0x1,
    TA_COMPLEX = 0x2,

} TYPE_ATTRIBUTE;

typedef struct type_dict_t TYPE_DICT;

typedef struct value_type_t {

    int baseId;
    int ptrLvl;

} VALUE_TYPE;

typedef union value_t {

    double d;
    int i;
    long l;
    void * ptr;
    char * str;
    VALUE_TYPE vt;

} VALUE;

typedef VALUE(readFunc)(char*);

typedef struct val_type_t {

    char * name;    //name of base type (int for int ***).
    int size;
    unsigned int attributes;
    int precedence;
    readFunc * func;

} VAL_TYPE;

typedef enum object_type_e {

    O_VARIABLE = 1,
    O_FUNCTION,

} OBJECT_TYPE;

int getTypeSize(VALUE_TYPE t);
int isFloatingPoint(VALUE_TYPE t);

unsigned int typeInsert(char * name, int size, unsigned int attributes, readFunc * func, int precedence);
unsigned int getTypeId(char * name);
VAL_TYPE getTypeInfo(unsigned int typeId);

void insertDefaultTypes();

VALUE typeConvert(VALUE_TYPE dest, VALUE_TYPE origin, VALUE val);
PRINT_VALUE getPrintValue(VALUE_TYPE valType, PRINT_TYPE * type, VALUE val);

VALUE_TYPE typeGetSmallestCommon(VALUE_TYPE t1, VALUE_TYPE t2);

#endif
