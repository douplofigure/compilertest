#include "util.h"

#include <stdlib.h>
#include <stdio.h>

#include "ast.h"
#include "hashmap.h"

#include "prettyprint.h"

void insertVariable(SYMBOL_TABLE * table, AST * ast) {


    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    char * name = astGetAttributeValue(children[0], A_VALUE).str;

    SYMBOL_INFO info;
    info.oType = O_VARIABLE;
    info.vType = astGetAttributeValue(ast, A_VALUE_TYPE).vt;
    info.name = name;
    info.memOffset = 0;

    symbolTableInsert(table, name, info);

}

void insertFunction(SYMBOL_TABLE * table, AST * ast) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);
    char * name = astGetAttributeValue(children[0], A_VALUE).str;

    SYMBOL_INFO info;
    info.oType = O_FUNCTION;
    info.vType = astGetAttributeValue(ast, A_VALUE_TYPE).vt;
    info.name = name;

    symbolTableInsert(table, name, info);

    SYMBOL_TABLE * fTable = utilCreateSymbolTable(children[2], table);

    int argCount;
    AST ** args = astGetChildren(children[1], &argCount);

    for (int i = 0; i < argCount; ++i) {
        insertVariable(fTable, args[i]);
    }

    astAddAttribute(ast, A_SYM_TABLE, (VALUE)(void*)fTable);

}

SYMBOL_TABLE * utilCreateSymbolTable(AST * ast, SYMBOL_TABLE * parent) {

    SYMBOL_TABLE * table = symbolTableCreate(parent);

    int childCount, tmp;
    AST ** children = astGetChildren(ast, &childCount);

    for (int i = 0; i < childCount; ++i) {

        switch (astGetType(children[i])) {

            case AST_FUNC_DECL:
                insertFunction(table, children[i]);
                break;

            case AST_VAR_DECLARATION:
                insertVariable(table, children[i]);
                break;

        }

    }

    return table;

}
