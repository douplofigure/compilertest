#ifndef _UTIL_H
#define _UTIL_H

#include "hashmap.h"
#include "ast.h"
#include "codestream.h"

#include <stdio.h>

SYMBOL_TABLE * utilCreateSymbolTable(AST * ast, SYMBOL_TABLE * parent);
CODE_STREAM * utilCreateCodeStream(AST * ast, SYMBOL_TABLE * globalSymbolTable);
void writeAssemblyToFile(FILE * file, CODE_STREAM * stream, SYMBOL_TABLE * globalSymbolTable);

#endif // _UTIL_H
